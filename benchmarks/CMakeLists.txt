# Pull in the project includes
include_directories(${PROJECT_SOURCE_DIR}/src)

# Put the resulting libraries in the `example` directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/benchmarks)

set(BENCHMARKSOURCES
   CACHE INTERNAL "benchmark sources"
)

#=====================================================================#
# Subdirectories                                                      #
#=====================================================================#

# Pull in the project includes
include_directories(${PROJECT_SOURCE_DIR}/src)
# Put the resulting libraries in the `example` directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/benchmarks)

# add subdirectories with models
add_subdirectory(queue)
add_subdirectory(phold)
add_subdirectory(devstone)
