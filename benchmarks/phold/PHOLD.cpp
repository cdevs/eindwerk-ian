#include "PHOLD.h"
#include <chrono>
#include <thread>

using namespace cdevs;

namespace cdevs_examples {
namespace phold {

PholdEvent::PholdEvent(int size)
{
	payload_ = std::vector<int> (size,7);
}

PHOLDModelState::PHOLDModelState()
	: State(), events_()
{
}

double getProcTime(std::shared_ptr<PholdEvent> event)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	return distribution(generator);
}

int getRand(std::shared_ptr<PholdEvent> event)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution(0, 60000);
	return distribution(generator);
}

int getNextDestination(std::shared_ptr<PholdEvent> event, int nodenum, std::vector<int> local, std::vector<int> remote,
        double percentageremote)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	if (distribution(generator) > percentageremote || remote.empty()) {
		std::uniform_int_distribution<int> distribution2(0, local.size() - 1);
		return local.at(distribution2(generator));
	} else {
		std::uniform_int_distribution<int> distribution2(0, remote.size() - 1);
		return remote.at(distribution2(generator));
	}
}

HeavyPHOLDProcessor::HeavyPHOLDProcessor(std::string name, unsigned iterations, unsigned totalAtomics, int modelnumber,
        std::vector<int> local, std::vector<int> remote, double percentageremotes, int eventsize)
	: Atomic(name), percentageRemote_(percentageremotes), outports_(), totalAtomics_(totalAtomics), modelnumber_(
	        modelnumber), iterations_(iterations), local_(local), remote_(remote), eventsize_(eventsize)
{
	inport_ = AddInPort("inport");
	state_ = std::unique_ptr<PHOLDModelState>(new PHOLDModelState());

	for (unsigned i = 0; i < totalAtomics; i++) {
		std::string name = "outport_" + std::to_string(i);
		outports_.push_back(AddOutPort(name));
	}
	std::shared_ptr<PholdEvent> ev = std::make_shared<PholdEvent>(eventsize);
	state_->events_.push_back(std::make_tuple(ev, getProcTime(ev)));
}

double HeavyPHOLDProcessor::TimeAdvance()
{
	if (!state_->events_.empty()) {
		return std::get<1>(state_->events_.front());
	} else {
		return std::numeric_limits<double>::infinity();
	}
}

PHOLDModelState & HeavyPHOLDProcessor::ExtTransition(Outbags inputs)
{
	if (!state_->events_.empty()) {
		std::get<1>(state_->events_.front()) -= elapsed_;
	}
	for (auto i : inputs.at(inport_)) {
		std::shared_ptr<const PholdEvent> k = std::dynamic_pointer_cast<const PholdEvent>(i);
		std::shared_ptr<PholdEvent> j = std::make_shared<PholdEvent>(k->payload_.size());
		state_->events_.push_back(std::make_tuple(j, getProcTime(j)));
		for (unsigned k = 0; k < iterations_; k++) {
			continue;
			//std::chrono::milliseconds timespan(5); // or whatever
			//std::this_thread::sleep_for(timespan); //TODO: does this keep the process busy?
		}
	}
	return *state_;
}

PHOLDModelState & HeavyPHOLDProcessor::IntTransition()
{
	if (state_->events_.empty()) {
		return *state_;
	} else {
		state_->events_.pop_front();
	}
	return *state_;
}

PHOLDModelState & HeavyPHOLDProcessor::ConfTransition(Outbags inputs)
{
	if (state_->events_.size() > 1) {
		state_->events_.pop_front();
	} else {
		state_->events_.clear();
	}
	for (auto& i : inputs.at(inport_)) {
		std::shared_ptr<const PholdEvent> k = std::dynamic_pointer_cast<const PholdEvent>(i);
		std::shared_ptr<PholdEvent> j = std::make_shared<PholdEvent>(k->payload_.size());
		state_->events_.push_back(std::make_tuple(j, getProcTime(j)));
		for (unsigned k = 0; k < iterations_; k++) {
			continue; //TODO: does this keep the process busy?
		}
	}
	return *state_;
}

Outbags HeavyPHOLDProcessor::OutputFunction()
{
	Outbags out;
	if (!state_->events_.empty()) {
		auto i = state_->events_.front();
		int next = getNextDestination(std::get<0>(i), modelnumber_, local_, remote_, percentageRemote_);
		Outbag evs = { std::make_shared<const PholdEvent>(eventsize_) };
		out.insert(std::make_pair(outports_.at(next), evs));
	}
	return out;
}

PHOLD::PHOLD(unsigned nodes, unsigned atomicsPerNode, unsigned iterations, double percentageremotes, int eventsize, bool distributed)
	: Coupled("PHOLD"), processors_(),eventsize_(eventsize), distributed_(distributed)
{
	distributed_ = false;
	int cntr = 0;
	unsigned totalAtomics = nodes * atomicsPerNode;
	std::vector<std::vector<int> > procs;
	for (unsigned node = 0; node < nodes; node++) {
		std::vector<int> l = std::vector<int>();
		for (unsigned i = 0; i < atomicsPerNode; i++) {
			l.push_back(atomicsPerNode * node + i);
		}
		procs.push_back(l);
	}
	unsigned e = 0;
	for (auto i : procs) {
		std::vector<int> allnoi;
		unsigned e2 = 0;
		for (auto j : procs) {
			if (e2 != e) {
				for (int el : j) {
					allnoi.push_back(el);
				}
			}
			++e2;
		}
		for (auto j : i) {
			std::vector<int> inoj = std::vector<int>(i);
			inoj.erase(std::remove(inoj.begin(), inoj.end(), j), inoj.end());
			std::string name = "Processor_" + std::to_string(cntr);
			std::shared_ptr<HeavyPHOLDProcessor> m = HeavyPHOLDProcessor::create(name, iterations,
			        totalAtomics, cntr, inoj, allnoi, percentageremotes, eventsize);
			AddSubModel(m, (distributed_ ? e : 0));
			processors_.push_back(m);
			++cntr;
		}
		++e;
	}
	for (unsigned i = 0; i < processors_.size(); i++) {
		for (unsigned j = 0; j < processors_.size(); j++) {
			if (i == j) {
				continue;
			} else {
				ConnectPorts(processors_.at(i)->output_ports_.at(j), processors_.at(j)->inport_);
			}
		}
	}

}

}
}
