/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "PHOLD.h"
#include "Simulator.h"

using namespace cdevs;
using cdevs_examples::phold::PHOLD;

bool TerminateWhenStateIsReached(DevsTime clock, BaseDevs * model)
{
	return false;
}

int main(int argc, char * argv[])
{
	int def_nodes = 2;
	int def_atomics = 2;
	int def_iterations = 5;
	double def_perc_remote = 0;
	double def_term_time = 5;
	int def_eventsize = 16;
	if (argc == 7) {
		def_nodes = std::atoi(argv[1]);
		def_atomics = std::atoi(argv[2]);
		def_iterations = std::atoi(argv[3]);
		def_perc_remote = std::atof(argv[4]);
		def_term_time = std::atof(argv[5]);
		def_eventsize = std::atoi(argv[6]);
	}
	std::shared_ptr < PHOLD > model = PHOLD::create(def_nodes, def_atomics, def_iterations, def_perc_remote, def_eventsize);

	Simulator sim(model, def_nodes);

	// sim.setTerminationCondition(std::function<bool(DevsTime, BaseDevs *)>(TerminateWhenStateIsReached));

	sim.set_termination_time(def_term_time);

	sim.set_checkpoint_name("phold");
	sim.set_checkpoint_interval(1);

	sim.set_gvt_interval(1);

	sim.set_verbose();
	sim.set_verbose("phold.txt");
	sim.set_xml("phold.xml");
	sim.set_json("phold.json");

	sim.Simulate();

	return 0;
}
