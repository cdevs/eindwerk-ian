/*
 * Queue.cpp
 *
 *  Created on: May 21, 2015
 *      Author: lotte
 */

#include "Queue.h"

using cdevs::Coupled;
using cdevs::Atomic;

namespace cdevs_examples {
namespace Queue {

double getRandom(int randomSeed, bool random)
{
	if (!random)
		return 0.5;
	long int seed = randomSeed ? randomSeed : rand();
	seed = (1664525 * seed + 1013904223) % 4294967296;
	return float(int(float(seed) / 4294967296 * 1000)) / 1000;
}

ProcessorEvent::ProcessorEvent(int eventSize)
	: eventSize_(eventSize)
{
}

ProcessorState::ProcessorState()
	: State(), event1Counter_(std::numeric_limits<double>::infinity()), event1_(), queue_()
{
}

ProcessorEvent::ProcessorEvent(const ProcessorEvent& other)
	: ProcessorEvent(other.eventSize_)
{
}

Processor::Processor(std::string name, bool random)
	: Atomic(name), random_(random)
{
	state_ = std::unique_ptr<ProcessorState>(new ProcessorState());
	recv_event1_ = AddInPort("in_event1");
	send_event1_ = AddOutPort("out_event1");
}

double Processor::TimeAdvance()
{
	return state_->event1Counter_;
}

ProcessorState& Processor::IntTransition()
{
	//if (state_->event1Counter_ == std::numeric_limits<double>::infinity())
	//	return *state_;
	state_->event1Counter_ -= TimeAdvance();
	if (state_->event1Counter_ == 0 && state_->queue_.empty()) {
		state_->event1Counter_ = std::numeric_limits<double>::infinity();
		state_->event1_.reset();
	} else {
		state_->event1Counter_ = getRandom(0, random_);
		state_->event1_ = state_->queue_.front();
		state_->queue_.pop_front();
	}
	return *state_;
}

ProcessorState& Processor::ExtTransition(Outbags inputs)
{
	state_->event1Counter_ -= elapsed_;
	for (auto const_ev : inputs.at(recv_event1_)) {
		auto ev = std::dynamic_pointer_cast<const ProcessorEvent>(const_ev);
		if (!state_->event1_.get()) {
			state_->event1_ = std::make_shared<ProcessorEvent>(*ev);
			state_->event1Counter_ = getRandom(0, random_);
		} else {
			state_->queue_.push_front(std::make_shared<ProcessorEvent>(*ev));
		}
	}
	return *state_;
}

Outbags Processor::OutputFunction()
{
	return { {	send_event1_, {state_->event1_}}};
}

Generator::Generator()
	: Atomic("Generator")
{
	state_ = std::unique_ptr<GeneratorState>(new GeneratorState("gen_event1"));
	send_event1_ = AddOutPort("out_event1");
}

double Generator::TimeAdvance()
{
	return 1.0;
}

GeneratorState const& Generator::IntTransition()
{
	return *state_;
}

Outbags Generator::OutputFunction()
{
	return { {	send_event1_, {std::make_shared<ProcessorEvent>(1)}}};
}

Queue::Queue(unsigned width, bool random)
	: Coupled("Queue"), random_(random)
{
	generator_ = Generator::create();
	AddSubModel(generator_);

	std::shared_ptr<Processor> prev;
	std::string name = "Processor0";
	std::shared_ptr<Processor> m = Processor::create(name, random);
	AddSubModel(m);
	ConnectPorts(generator_->send_event1_, m->recv_event1_);
	prev = m;
	for (unsigned i = 1; i < width; i++) {
		name = "Processor" + std::to_string(i);
		std::shared_ptr<Processor> m = Processor::create(name, random);
		AddSubModel(m);
		ConnectPorts(prev->send_event1_, m->recv_event1_);
		prev = m;
	}
}
}
}
