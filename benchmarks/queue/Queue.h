#ifndef BENCHMARKS_QUEUE_H_
#define BENCHMARKS_QUEUE_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <limits>
#include <cstdlib>
#include <sstream>
#include <memory>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/common.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/details/polymorphic_impl.hpp>

namespace cdevs_examples {
namespace Queue {

typedef std::list<std::shared_ptr<const cdevs::EventBase> > Outbag;
typedef std::map<std::weak_ptr<cdevs::Port>, Outbag, std::owner_less<std::weak_ptr<cdevs::Port> > > Outbags;

double getRandom(int randomSeed = 0, bool random = true);

class ProcessorEvent: public cdevs::EventCRTP<ProcessorEvent, 0>
{
	friend class cereal::access;
public:
	ProcessorEvent(int eventSize = -1);
	ProcessorEvent(const ProcessorEvent& other);
	std::string string() const
	{
		return std::to_string(eventSize_);
	}
protected:
	int eventSize_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::EventCRTP<ProcessorEvent, 0> >(this), eventSize_);
	}
};

class ProcessorState: public cdevs::State<ProcessorState>
{
	friend class Processor;
	friend class cereal::access;
public:
	ProcessorState();
	std::string string() const
	{
		std::string str = "Event1: ";
		if (event1_.get()) {
			str += event1_->string();
		}
		str += "\tQueue: ";
		for (auto proc_event : queue_) {
			str += proc_event->string();
			str += " ";
		}
		return str;
	}
protected:
	double event1Counter_;
	std::shared_ptr<ProcessorEvent> event1_;
	std::list<std::shared_ptr<ProcessorEvent>> queue_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<ProcessorState> >(this), event1Counter_, event1_, queue_);
	}
};

class Processor: public cdevs::Atomic<Processor, ProcessorState>
{
	friend class Queue;
	friend class cereal::access;
public:
	Processor(std::string name = "", bool random = true);

	ProcessorState& ExtTransition(Outbags inputs);
	ProcessorState& IntTransition();
	Outbags OutputFunction();
	double TimeAdvance();
protected:
	std::weak_ptr<cdevs::Port> recv_event1_;
	std::weak_ptr<cdevs::Port> send_event1_;
	bool random_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Processor, ProcessorState>>(this), recv_event1_, send_event1_,
		        random_);
	}
};

class GeneratorState: public cdevs::State<GeneratorState>
{
	friend class Generator;
	friend class cereal::access;
public:
	GeneratorState(std::string str = "")
		: State(), str_(str)
	{
	}
	std::string string() const
	{
		return str_;
	}
protected:
	std::string str_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<GeneratorState> >(this), str_);
	}
};

class Generator: public cdevs::Atomic<Generator, GeneratorState>
{
	friend class Queue;
	friend class cereal::access;
public:
	Generator();

	GeneratorState const& IntTransition();
	Outbags OutputFunction();
	double TimeAdvance();
protected:
	std::weak_ptr<cdevs::Port> send_event1_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Generator, GeneratorState>>(this), send_event1_);
	}
};

class Queue: public cdevs::Coupled<Queue>
{
	friend class cereal::access;
public:
	Queue(unsigned width, bool random = true);
private:
	Queue()
		: Coupled(""), random_(true)
	{
	}
	std::shared_ptr<Generator> generator_;
	std::shared_ptr<Processor> processor_;
	bool random_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Coupled<Queue> >(this), generator_, processor_, random_);
	}

};
}
} /* namespace cdevs_examples */

CEREAL_REGISTER_TYPE(cdevs_examples::Queue::ProcessorEvent)
CEREAL_REGISTER_TYPE(cdevs_examples::Queue::Generator)
CEREAL_REGISTER_TYPE(cdevs_examples::Queue::Processor)
CEREAL_REGISTER_TYPE(cdevs_examples::Queue::Queue)

#endif /* BENCHMARKS_QUEUE_H_ */
