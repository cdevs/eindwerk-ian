/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "Queue.h"
#include "Simulator.h"

using namespace cdevs;
using cdevs_examples::Queue::Queue;

bool TerminateWhenStateIsReached(DevsTime clock, BaseDevs * model)
{
	return false;
}

int main(int argc, char * argv[])
{
	unsigned def_models = 2;
	unsigned def_term_time = 5;
	bool def_random = false;
	std::shared_ptr<Queue> model;
	if(argc == 4){
		def_models = std::atoi(argv[1]);
		def_term_time = std::atoi(argv[2]);
		def_random = std::atoi(argv[3]);
	}
	model = Queue::create(def_models, def_random);

	Simulator sim(model);

	// TODO: give termination time priority to condition?
	// sim.setTerminationCondition(std::function<bool(DevsTime, BaseDevs *)>(TerminateWhenStateIsReached));

	sim.set_termination_time(def_term_time);

	sim.set_checkpoint_name("queue");
	sim.set_checkpoint_interval(1);

	sim.set_gvt_interval(1);

	sim.set_verbose();
	sim.set_verbose("queue.txt");
	sim.set_xml("queue.xml");
	sim.set_json("queue.json");

	sim.set_classic_devs();

	sim.Simulate();

	return 0;
}
