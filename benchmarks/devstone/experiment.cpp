/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "DEVStone.h"
#include "Simulator.h"

using namespace cdevs;
using cdevs_examples::devstone::DEVStone;

bool TerminateWhenStateIsReached(DevsTime clock, BaseDevs * model)
{
	return false;
}

int main(int argc, char * argv[])
{
	unsigned def_width = 50;
	unsigned def_depth = 5;
	bool def_random = false;
	double def_term_time = 10;
	if (argc == 5) {
		def_width = std::atoi(argv[1]);
		def_depth = std::atoi(argv[2]);
		def_random = std::atoi(argv[3]);
		def_term_time = std::atof(argv[4]);
	}
	std::shared_ptr<DEVStone> model = DEVStone::create(def_width, def_depth, def_random);

	Simulator sim(model, 2);

	// sim.setTerminationCondition(std::function<bool(DevsTime, BaseDevs *)>(TerminateWhenStateIsReached));

	sim.set_termination_time(def_term_time);

	sim.set_checkpoint_name("devstone");
	sim.set_checkpoint_interval(1);

	sim.set_gvt_interval(5);

	sim.set_verbose();
	sim.set_verbose("devstone.txt");
	sim.set_xml("devstone.xml");
	sim.set_json("devstone.json");

	sim.set_classic_devs();

	sim.Simulate();

	return 0;
}
