/*
 * Queue.cpp
 *
 *  Created on: May 21, 2015
 *      Author: lotte
 */

#include "DEVStone.h"

namespace cdevs_examples {
namespace devstone {

double getRandom(int randomSeed)
{
	long int seed = randomSeed ? randomSeed : rand();
	seed = (1664525 * seed + 1013904223) % 4294967296;
	return float(int(float(seed) / 4294967296 * 1000)) / 1000;
}

ProcessorEvent::ProcessorEvent(int eventSize)
	: eventSize_(eventSize)
{
}

ProcessorEvent::ProcessorEvent(const ProcessorEvent& other)
	: ProcessorEvent(other.eventSize_)
{
}

ProcessorState::ProcessorState()
	: State(), event1Counter_(std::numeric_limits<double>::infinity()), event1_(), queue_()
{
}

Processor::Processor(std::string name, bool randomta)
	: Atomic(name), randomta_(randomta)
{
	state_ = std::unique_ptr < ProcessorState > (new ProcessorState());
	recv_event1_ = AddInPort(name + "_in_event1");
	send_event1_ = AddOutPort(name + "_out_event1");
}

double Processor::TimeAdvance()
{
	return state_->event1Counter_;
}

ProcessorState& Processor::IntTransition()
{
	state_->event1Counter_ -= TimeAdvance();
	if (state_->event1Counter_ == 0 && state_->queue_.empty()) {
		state_->event1Counter_ = std::numeric_limits<double>::infinity();
		state_->event1_.reset();
	} else {
		state_->event1Counter_ = randomta_ ? getRandom() : 1;
		state_->event1_ = state_->queue_.front();
		state_->queue_.pop_front();
	}
	return *state_;
}

ProcessorState& Processor::ExtTransition(Outbags inputs)
{
	state_->event1Counter_ -= elapsed_;
	for (auto const_ev : inputs.at(recv_event1_)) {
		auto ev = std::dynamic_pointer_cast<const ProcessorEvent>(const_ev);
		if (!state_->event1_.get()) {
			state_->event1_ = std::make_shared < ProcessorEvent > (*ev);
			state_->event1Counter_ = getRandom();
		} else {
			state_->queue_.push_front(std::make_shared < ProcessorEvent > (*ev));
		}
	}
	return *state_;
}

Outbags Processor::OutputFunction()
{
	//Outbag out_ev = {state_->event1_};
	//Outbags out = {{send_event1_, out_ev}};
	return { {	send_event1_, {state_->event1_}}};
}

Generator::Generator()
	: Atomic("Generator")
{
	state_ = std::unique_ptr < GeneratorState > (new GeneratorState("gen_event1"));
	send_event1_ = AddOutPort("generator_out_event1");
}

double Generator::TimeAdvance()
{
	return 1.0;
}

GeneratorState& Generator::IntTransition()
{
	return *state_;
}

Outbags Generator::OutputFunction()
{
	//Outbag evs = {std::make_shared < ProcessorEvent > (1)};
	//Outbags out = {{send_event1_, evs}};
	return { {	send_event1_, {std::make_shared < ProcessorEvent > (1)}}};
}

CoupledRecursion::CoupledRecursion(unsigned width, unsigned depth, bool randomta)
	: Coupled("Coupled" + std::to_string(depth)), randomta_(randomta)
{
	recv_event1_ = AddInPort("CoupledRecursion_in_event1");
	send_event1_ = AddOutPort("CoupledRecursion_out_event1");

	if (depth > 1) {
		recurse_ = CoupledRecursion::create(width, depth - 1, randomta);

		AddSubModel(recurse_);
		ConnectPorts(recv_event1_, recurse_->recv_event1_);
	}
	std::shared_ptr<Processor> prev;
	for (unsigned i = 0; i < width; i++) {
		std::string name = "Processor" + std::to_string(depth) + "_" + std::to_string(i);
		std::shared_ptr<Processor> p = Processor::create(name, randomta);
		AddSubModel(p);
		if (i == 0) {
			if (depth > 1) {
				ConnectPorts(recurse_->send_event1_, p->recv_event1_);
			} else {
				ConnectPorts(recv_event1_, p->recv_event1_);
			}
		} else {
			ConnectPorts(prev->send_event1_, p->recv_event1_);
		}
		prev = p;
	}
	ConnectPorts(prev->send_event1_, send_event1_);
}

DEVStone::DEVStone(unsigned width, unsigned depth, bool randomta)
	: Coupled("DEVStone"), randomta_(randomta)
{
	generator_ = Generator::create();
	AddSubModel(generator_);
	recurse_ = CoupledRecursion::create(width, depth, randomta);
	AddSubModel(recurse_);
	ConnectPorts(generator_->send_event1_, recurse_->recv_event1_);
}

}
}
