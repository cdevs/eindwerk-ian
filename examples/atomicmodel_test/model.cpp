/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include "../../src/exceptions/RuntimeDevsException.h"
#include <algorithm>
#include <limits>

using namespace cdevs;

namespace cdevs_examples
{
namespace atomic
{
TrafficLightMode::TrafficLightMode(std::string value) :
	value_(value) {
}

TrafficLight::TrafficLight(std::string name) :
		Atomic(name) {
	setState( state_red_ );
	elapsed_ = 1.5;

	INTERRUPT = AddInPort("INTERRUPT");
	OBSERVED = AddOutPort("OBSERVED");
}

TrafficLightMode const& TrafficLight::ExtTransition(
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<Port> > > inputs) {
	throw cdevs::RuntimeDevsException("AtomicModel_Test shouldn't be transitioning externally!");
}

TrafficLightMode const& TrafficLight::IntTransition() {
	std::string state = getState().getValue();

	if (state == "red")
		return state_green_;
	else if (state == "green")
		return state_yellow_;
	else if (state == "yellow")
		return state_red_;
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<Port> > > TrafficLight::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "red")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("grey")}}};
	else if (state == "green")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("yellow")}}};
	else if (state == "yellow")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("grey")}}};
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

double TrafficLight::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "red")
		return 60;
	else if (state == "green")
		return 50;
	else if (state == "yellow")
		return 10;
	else if (state == "manual")
		return std::numeric_limits<double>::infinity();

	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

}
}
