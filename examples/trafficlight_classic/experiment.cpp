/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"
#include "TracerCustom.h"

#include <sstream>

#include <cereal/archives/binary.hpp>

using namespace cdevs;
using namespace cdevs_examples::classic;

bool TerminateWhenStateIsReached(DevsTime clock, std::shared_ptr<BaseDevs> model)
{
	std::shared_ptr<TrafficLight> tl = std::dynamic_pointer_cast<TrafficLight>(model);
	if (tl) {
		return (tl->getState().getValue() == "manual");
	}
	return false;
}

int main(int argc, char * argv[]) {
	std::shared_ptr<TrafficSystem> model = TrafficSystem::create("trafficSystem");

	Simulator sim(model);

	// TODO: give termination time priority to condition?
	// sim.setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)>(TerminateWhenStateIsReached));

	sim.set_termination_time(400.0);

//	std::shared_ptr<TracerCustom> tracer = std::make_shared<TracerCustom>();
//	sim.set_custom_tracer(tracer);

	sim.set_verbose("classic.txt");
	sim.set_xml("classic.xml");
	sim.set_verbose();
//	sim.setJson("classic.json");
	sim.set_classic_devs();
	sim.set_scheduler_heap();
//	sim.setSchedulerSortedList();
//	sim.setSchedulerList();
	sim.Simulate();

	std::cout << "\nSimulation terminated with traffic light in state " << model->getTrafficLight()->getState().getValue() << std::endl;


	return 0;
}
