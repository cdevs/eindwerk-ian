/*
 * city_models.h
 *
 *  Created on: 5-jun.-2015
 *      Author: david
 */

#ifndef EXAMPLES_CITY_MODEL_H_
#define EXAMPLES_CITY_MODEL_H_

#include "traffic_models.h"
#include <random>

namespace cdevs_examples {
namespace traffic {

class City : public cdevs::Coupled<City>
{
public:
	City();
private:
	std::shared_ptr<Collector> collector_;
};

}
}

#endif /* EXAMPLES_TRAFFIC_MODELS_H_ */
