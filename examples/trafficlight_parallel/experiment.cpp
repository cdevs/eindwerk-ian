/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"
#include <iostream>

#include <cereal/archives/binary.hpp>

using namespace cdevs;
using namespace cdevs_examples::parallel;

bool TerminateWhenStateIsReached(DevsTime clock, std::shared_ptr<BaseDevs> model)
{
	std::shared_ptr<TrafficSystem> ts =  std::dynamic_pointer_cast<TrafficSystem>(model);
	if(ts)
	{
		std::shared_ptr<TrafficLight> tl = ts->getTrafficLight();
		if (tl) {
			return (tl->getState().getValue().compare("manual") == 0);
		}
	}
	return false;
}

int main(int argc, char * argv[]) {
	std::shared_ptr<TrafficSystem> model = TrafficSystem::create("trafficSystem");

	Simulator sim(model, 2);

//	sim.set_termination_condition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)>(TerminateWhenStateIsReached));

	sim.set_termination_time(100000.0);

	sim.set_checkpoint_name("parallel");
	sim.set_checkpoint_interval(1);

	sim.set_gvt_interval(1);

	sim.set_verbose();

	sim.Simulate();
	
	std::cout << "\nSimulation terminated with traffic light in state " << model->getTrafficLight()->getState().getValue() << std::endl;

	return 0;
}
