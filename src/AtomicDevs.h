/*
 * AtomicDevs.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef ATOMICDEVS_H_
#define ATOMICDEVS_H_

#include <list>
#include <map>
#include <memory>
#include <stack>
#include <string>

#include "../lib/cereal-1.1.0/include/cereal/details/polymorphic_impl.hpp"
#include "../lib/cereal-1.1.0/include/cereal/details/static_object.hpp"
#include "../lib/cereal-1.1.0/include/cereal/types/base_class.hpp"
#include "../lib/cereal-1.1.0/include/cereal/types/polymorphic.hpp"
#include "BaseDEVS.h"
#include "DevsTime.h"
#include "utility.h"

namespace cereal {
class access;
} /* namespace cereal */

namespace cdevs {

/**
 * Abstract base class for all atomic-DEVS descriptive classes.
 */
class AtomicDevs: public BaseDevs
{
	friend class Solver;
public:
	AtomicDevs(std::string name);
	virtual ~AtomicDevs();

	virtual StateBase& getState() const = 0;
	// template<class T, class S>
	virtual StateBase& getState(DevsTime request_time, bool first_call = true) const
	{
		return getState();
	}
	;

	// template<class T, class S>
	virtual void setState(StateBase const& state) = 0;

	void set_GVT(double new_gvt, bool last_state_only); // should also pass a reference to a container holding all activities.
	virtual StateBase const& ExtTransition(
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > inputs) = 0;
	virtual StateBase const& IntTransition() = 0;
	virtual StateBase const& ConfTransition(
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > inputs) = 0;
	virtual std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > OutputFunction();
	virtual double TimeAdvance();

	void FlattenConnections();
	void UnflattenConnections();
	void set_location(int location, bool force);

	int Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
	        std::list<std::shared_ptr<BaseDevs>> select_hierarchy);

	bool Revert(DevsTime time);

	void AddDelayedTransition(DevsTime time);
	bool PerformDelayedTransition(DevsTime until_time);

	void TakeSnapshot();
	void TakeInitialSnapshot();
private:
	AtomicDevs()
		: AtomicDevs("")
	{
	}
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<BaseDevs>(this), relocatable_, last_read_time_, select_hierarchy_,
		        delayed_transitions_);
	}

	bool relocatable_;
	DevsTime last_read_time_;
	std::list<std::shared_ptr<BaseDevs>> select_hierarchy_;
	std::stack<DevsTime> delayed_transitions_;
};

template<class Derived, class StateType = StateBase>
class Atomic: public DevsCRTP<AtomicDevs, Derived>
{
public:
	using DevsCRTP<AtomicDevs, Derived>::DevsCRTP;

	virtual ~Atomic()
	{
	}
	;

	/**
	 * \brief Gets the state of Atomic
	 *
	 * @return State of Atomic
	 */
	// template <class T, class S>
	// State<T, S> const& AtomicDevs::getState() const
	StateType& getState() const
	{
		return *state_;
	}

	/**
	 * \brief Sets the state of Atomic
	 *
	 * TODO: We should think this downcast through. We could hide the constructor of
	 * StateBase or try taking State as argument to this method. We could also simply
	 * throw an error if state doesn't inherit State.
	 *
	 * @return State of Atomic
	 */
	void setState(StateBase const& state)
	{
		state_.reset(static_cast<StateType *>(&state.clone()));
	}

	virtual StateType const& ExtTransition(
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > inputs)
	{
		return *state_;
	}

	virtual StateType const& IntTransition()
	{
		return *state_;
	}

	virtual StateType const& ConfTransition(
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > inputs)
	{
		return *state_;
	}
protected:
	std::unique_ptr<StateType> state_;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<DevsCRTP<AtomicDevs, Derived>>(this), state_);
	}
};
} /* namespace ns_DEVS */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::AtomicDevs)

#endif /* ATOMICDEVS_H_ */
