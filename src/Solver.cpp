#include <algorithm>
#include "Solver.h"
#include "Controller.h"
#include <cassert>
#include <limits>

namespace cdevs {

std::mutex Solver::temp_mutex_;

/**
 * \brief Constructor
 */
Solver::Solver(unsigned id)
	: id_(id), msg_copy_(0), block_outgoing_(DevsTime(-1, -1)), model_(0)
{
	has_initial_allocator_ = false;
	is_simulation_irreversible_ = true;
	do_some_tracing_ = false;
	temporarily_irreversible_ = false;
	use_classic_devs_ = false;
	checkpoint_restored_ = false;
	tmin_ = std::numeric_limits<double>::infinity();
	message_received_count_ = 0;
	message_sent_count_ = 0;
	color_ = kWhite1;

	tracers_ = std::make_shared<Tracers>();
}

/**
 * \brief Default constructor (private, used for serializing).
 */
Solver::Solver() : Solver(0)
{
}

/**
 * Destructor
 */
Solver::~Solver()
{
}

/**
 * \brief Sets the controller of the solver
 *
 * @param controller  The controller of the solver
 */
void Solver::set_controller(std::weak_ptr<Controller> controller)
{
	controller_ = controller;
}

/**
 * \brief Wrapper for the AtomicDEVS output function, which will save event counts
 *
 * @param aDEVS the AtomicDEVS model that generates the output
 * @param time the time at which the output must be generated
 * @return The generated output
 */
std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >, std::owner_less<std::weak_ptr<Port> > > Solver::AtomicOutputGenerationEventTracing(
        std::shared_ptr<AtomicDevs> adevs, DevsTime time)
{
	// generate the atomic outputs
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > retval = AtomicOutputGeneration(adevs, time);

	// keep a copy that will story the event counts
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > newmap;

	for (auto iterator = retval.begin(); iterator != retval.end(); iterator++) {
		auto p = iterator->first.lock();
		auto pm = iterator->second;
		p->set_msg_count(p->get_msg_count() + pm.size());
		newmap.insert(std::pair<std::shared_ptr<Port>, std::list<std::shared_ptr<const EventBase> > >(p, pm));
	}
	return newmap;

}

/**
 * AtomicDEVS function to generate output, invokes the outputFnc function of the model.
 *
 * @param aDEVS: the AtomicDEVS model that generates the output
 * @param time: the time at which the output must be generated
 * @return The generated output
 */
std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >, std::owner_less<std::weak_ptr<Port> > > Solver::AtomicOutputGeneration(
        std::shared_ptr<AtomicDevs> adevs, DevsTime time)
{
	// get the model's output
	adevs->my_output_ = adevs->OutputFunction();

	// add an internal transition
	transitioning_[adevs->get_model_id()] |= 1;

	return adevs->my_output_;
}

/**
 * \brief Executes all given atomic transitions
 *
 * @param trans  The map containing the transitions of the atomic models
 * @param clock_now  The current clock value
 */
void Solver::MassAtomicTransitions(std::map<int, int> trans, DevsTime clock_now)
{
	// shorthands
	double t = clock_now.get_x();
	double age = clock_now.get_y();

	for (auto adevs_pair : trans) {
		int ttype = adevs_pair.second;

		// find the model
		std::shared_ptr<AtomicDevs> adevs = FindDevsModelById(adevs_pair.first);

		// if we can't find it, then there's something wrong, so throw an exception
		if (adevs == nullptr) {
			throw RuntimeDevsException(
			        "[Solver::MassAtomicTransitions] got a null atomic devs for id " + std::to_string(adevs_pair.first));
		}

		if (ttype == 1) {
			// Internal only
			adevs->setState(adevs->IntTransition());
		} else if (ttype == 2) {
			// External only
			adevs->set_time_elapsed(t - adevs->get_time_last().get_x());
			StateBase const& new_state = adevs->ExtTransition(adevs->my_input_);
			adevs->setState(new_state);
		} else if (ttype == 3) {
			// Confluent
			adevs->set_time_elapsed(0.0);
			StateBase const& new_state = adevs->ConfTransition(adevs->my_input_);
			adevs->setState(new_state);

		} else {
			throw RuntimeDevsException("[Solver::MassAtomicTransitions] invalid transition");
		}

		// perform a time advance for the atomic model
		double ta = adevs->TimeAdvance();

		// update the last time seen
		adevs->time_last_ = clock_now;

		// we can't advance backwards, so throw an exception
		if (ta < 0) {
			throw RuntimeDevsException(
			        "[Solver::MassAtomicTransitions]  Negative time advance in atomic model '" + adevs->get_model_full_name() + "' with value "
			                + std::to_string(ta) + " at time " + std::to_string(t));
		}

		// Update the time, this is just done in the timeNext, as this will propagate to the basesimulator
		DevsTime time_next = DevsTime(t + ta, !ta ? 1 : age + 1);
		adevs->set_time_next(time_next);

		// if the simulation is reverisble, take a snapshot to store the old states
		if (!temporarily_irreversible_) {
			adevs->TakeSnapshot();
		}

		// this is different than pypdevs (again), just check the tracing here - Ian
		if (do_some_tracing_) {
			switch (ttype) {
			case 1: {
				tracers_->TracesInternal(adevs);
				break;
			}
			case 2: {
				tracers_->TracesExternal(adevs);
				break;
			}
			case 3: {
				tracers_->TracesConfluent(adevs);
				break;
			}
			default: {
				throw RuntimeDevsException(
				        "[Solver::MassAtomicTransitions] Got unidentified transition");
			}
			}
		}

		adevs->my_input_.clear(); // TODO: This causes lots of problems with parallel... - Ian
	}
}

/**
 * \brief Initializes the given atomic models
 *
 * @param adevs  The atomic models to intialize
 * @param at_time  The current clock value
 */
void Solver::AtomicInitialize(std::shared_ptr<AtomicDevs> adevs, DevsTime at_time)
{
	// recalculate the clock based on the elapsed time
	DevsTime t;
	t.set_x(at_time.get_x() - adevs->get_time_elapsed());
	t.set_y(1);

	// calculate the time advance value
	double ta = adevs->TimeAdvance();

	// we can't advance backwards, so throw an exception
	if (ta < 0) {
		throw RuntimeDevsException(
		        "[Solver::AtomicInitialize] Negative time advance in atomic model '" + adevs->get_model_full_name() + "' with value "
		                + std::to_string(ta) + " at initialisation");
	}

	// update the time next
	t.set_x(t.get_x() + ta);
	adevs->set_time_next(t);

	adevs->TakeInitialSnapshot();

	// call the tracers to perform the initialize tracing
	tracers_->TracesInitialize(adevs, at_time);
}

/**
 * \brief CoupledDEVS function to generate the output, calls the atomicDEVS models where necessary. Output is routed too.
 *
 * @param at_time  The time at which output should be generated
 * @return  The models that should be rescheduled
 */
std::list<std::shared_ptr<AtomicDevs> > Solver::CoupledOutputGenerationClassic(DevsTime at_time)
{
	// get the models which have to occur now
	std::list<std::shared_ptr<AtomicDevs> > immenent = model_->get_scheduler()->GetImminent(at_time);

	if (immenent.empty()) {
		// no immenent models, return everything in the transitioning right now
		std::list<std::shared_ptr<AtomicDevs> > transitioning_list_;

		// push every model to the transitioning list
		for (auto id_transitioning_pair : transitioning_) {
			transitioning_list_.push_back(FindDevsModelById(id_transitioning_pair.first));
		}

		return transitioning_list_;
	}

	// reschedule all immenent models
	std::list<std::shared_ptr<AtomicDevs> > reschedule = immenent;
	for (auto& model : immenent) {
		// recalculate the time next
		DevsTime current_time_next = model->get_time_next();
		DevsTime time_next = DevsTime(current_time_next.get_x(), current_time_next.get_y() + 1);

		// update the time next
		model->set_time_next(time_next);
	}

	// get the most immenent child
	std::shared_ptr<AtomicDevs> child;

	if (immenent.size() > 1) {
		immenent.sort(wayToSort);

		std::list<std::shared_ptr<AtomicDevs> > pending = immenent;

		int level = 1;

		while (pending.size() > 1) {
			// take the model each time,
			// as we need to make sure that the select
			// hierarchy is valid everywhere
			std::shared_ptr<BaseDevs> model = pending.front();
			pending.pop_front();

			level += 1;
		}
		child = pending.front();
	} else
		child = immenent.front();

	// recorrect the timeNext of the model that will transition
	DevsTime current_time_next = child->get_time_next();
	DevsTime time_next = DevsTime(current_time_next.get_x(), current_time_next.get_y() - 1);

	// update the time next
	child->set_time_next(time_next);

	// calculate the output
	child->my_output_ = ClassicDEVSWrapper(child).OutputFunc();

	// copy the output
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > outbag = child->my_output_;

	// add an internal transition for the model
	transitioning_[child->get_model_id()] = 1;

	// check for external transitions
	for (auto outport_output_pair : outbag) {
		std::shared_ptr<Port> outport = outport_output_pair.first.lock();
		std::list<std::shared_ptr<const EventBase> > payload = outport_output_pair.second;

		// check the outline for each outport
		for (auto inport_z_pair : outport->routing_outline_) {
			std::shared_ptr<Port> inport = inport_z_pair.first.lock();
			std::function<Event(Event)> z_function = inport_z_pair.second;

			if (z_function) {
				// TODO payload = [z(pickle.loads(pickle.dumps(m))) for m in payload] - Ian
			}


			std::shared_ptr<AtomicDevs> host_devs = std::dynamic_pointer_cast<AtomicDevs>(
			        inport->GetHostDEVS().lock());

			// check if the host devs is a null pointer, throw an exception if so
			if (host_devs == nullptr) {
				throw RuntimeDevsException("[Solver::CoupledOutputGenerationClassic] Got a null pointer for the host devs");
				continue;
			}

			// clear the input of the host devs for the current inport
			host_devs->my_input_[inport].clear();

			// push the event based on the payload of the outport
			for (std::shared_ptr<const EventBase> payload_event : payload) {
				host_devs->my_input_[inport].push_back(payload_event);
			}


			// add an external transition
			transitioning_[host_devs->get_model_id()] = 2;

			// push the host devs so that it is rescheduled
			reschedule.push_back(host_devs);
		}
	}
	return reschedule;

}

/**
 * \brief Compares two BaseDevs to sort them properly
 *
 * The BaseDevs are compared based on their time next. This is used to
 * sort the BaseDevs properly in data structures
 *
 * @param one  The first BaseDevs to compare with the second
 * @param two  The second BaseDevs to compare with the first
 * @return  True if the first one is smaller than the second one, false otherwise
 */
bool wayToSort(std::shared_ptr<BaseDevs> one, std::shared_ptr<BaseDevs> two)
{
	return one->get_time_next() < two->get_time_next();
}

/**
 * \brief CoupledDEVS function to generate the output, calls the atomicDEVS models where necessary. Output is routed too.
 *
 * @param Time  The time at which output should be generated
 * @return  The models that should be rescheduled
 */
std::list<std::shared_ptr<AtomicDevs> > Solver::CoupledOutputGeneration(DevsTime at_time)
{
	// keep track of the remote atomic models
	std::map<std::shared_ptr<AtomicDevs>,
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > > > remotes;

	// generate the output for the immenent atomic models
	for (auto& devs : model_->get_scheduler()->GetImminent(at_time))
	{
		// generate the atomic output for the current immenent model
		auto outbag = AtomicOutputGeneration(devs, at_time);

		// check each output port to check for external transitions
		for (auto port_output_pair : outbag)
		{
			std::shared_ptr<Port> outport = port_output_pair.first.lock();
			auto payload = port_output_pair.second; // std::list<std::string>

			// check the outline of the outport to update the host devs
			for (auto port_z_pair : outport->routing_outline_)
			{
				// get the inport and the z function
				std::shared_ptr<Port> inport = port_z_pair.first.lock();
				std::function<Event(Event)> z_function = port_z_pair.second;

				// get the host devs
				std::shared_ptr<AtomicDevs> host_devs = std::dynamic_pointer_cast<AtomicDevs>(
				        inport->GetHostDEVS().lock());

				// if the function is assigned
				if (z_function)
				{
					//payload = [z(pickle.loads(pickle.dumps(m)))
					//for m in payload]
				}

				// if the model belongs to this kernel, update the input
				if (host_devs->get_location() == id_)
				{
					host_devs->my_input_[inport].insert(host_devs->my_input_[inport].end(),
					        payload.begin(), payload.end());
					transitioning_[host_devs->get_model_id()] |= 2;
				}
				else
				{
					// the model belongs to another kernel,
					// so add them to the remote map to send messages later
					remotes[host_devs][inport] = payload;
				}
			}
		}
	}

	// checks the remote map for messages that need to be send
	for (auto adevs_content_pair : remotes)
	{
		// get the atomic model
		std::shared_ptr<AtomicDevs> adevs = adevs_content_pair.first;

		// send the message
		Send(adevs, at_time, adevs_content_pair.second);
	}

	// keep track of the models that need rescheduling
	std::list<std::shared_ptr<AtomicDevs> > reschedule;
	for (auto id_transition_pair : transitioning_)
	{
		// find the model
		std::shared_ptr<AtomicDevs> model = FindDevsModelById(id_transition_pair.first);

		// make sure we don't add null pointers
		if (model != nullptr)
			reschedule.push_back(model);
	}
	return reschedule;
}

/**
 * \brief CoupledDEVS function to initialise the model, calls all its _local_ children too.
 */
void Solver::CoupledInitialize()
{
	DevsTime time_next(std::numeric_limits<double>::infinity(), 1);

	//for each local model (pypdevs) = for each model (CDevs)
	for (auto& devs : model_->get_component_set()) {
		// initialize atom devs
		AtomicInitialize(devs, DevsTime(0.0, 0));

		// get minimum to time next, which we will set our model to
		if (devs->get_time_next() < time_next) {
			time_next = devs->get_time_next();
		}
	}

	// update the time next
	model_->set_time_next(time_next);

	// set the scheduler type
	model_->set_scheduler(model_->get_scheduler_type());
}

/**
 * Registers a tracer
 *
 * @param tracer  The tracer to register
 * @param recover  Are we recovering simulation?
 */
void Solver::RegisterTracer(std::shared_ptr<Tracer> tracer)
{
	// make sure we set the flag that tells us to perform tracing
	do_some_tracing_ = true;

	// register the tracer
	tracers_->RegisterTracer(tracer);
}

/**
 * Finds the local model corresponding to the given id
 *
 * @return  A shared pointer to the local model, if the id is not found, a nullptr will be returned
 */
std::shared_ptr<AtomicDevs> Solver::FindDevsModelById(int id)
{
	for (auto& atomic_devs : models_)
	{
		// if the id matches the given id, we can return the found model
		if (atomic_devs->get_model_id() == id)
			return atomic_devs;
	}

	// no model found, return a null pointer
	return nullptr;
}

/**
 * \brief Sends message with content to the appropriate kernels
 *
 * @param adevs The model for the output
 * @param time The time related to the content
 * @param content The content, a map with Port as key and a string list as values
 */
void Solver::Send(std::shared_ptr<AtomicDevs> adevs, DevsTime time,
        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
                std::owner_less<std::weak_ptr<Port> > > content)
{
	if (block_outgoing_ == time and !checkpoint_restored_)
		return;

	int destination = adevs->get_location();

	Message msg(time, adevs->get_model_id(), content, color_);

	NotifySend(destination, time, color_);

	output_message_queue_.push_back(msg);

	controller_.lock()->get_kernel(destination)->Receive(msg);
}

/**
 * Sets the use of Classic DEVS instead of Parallel DEVS.
 *
 * @param classicDEVS: whether or not to use Classic DEVS
 */
void Solver::set_classic_devs(bool classic_devs)
{
	use_classic_devs_ = classic_devs;
}

/**
 * \brief Notify the simulation kernel of the sending of a message. Needed for GVT calculation.
 *
 * @param destination The name of the simulation kernel that will receive the sent message
 * @param timestamp  Simulation time at which the message is sent
 * @param color  Color of the message being sent (for Mattern's algorithm)
 */
void Solver::NotifySend(int destination, DevsTime timestamp, Color color)
{
	// update the sent count
	message_sent_count_++;

	// check if there's already an element in the mapping
	if (colorVectors_[color].count(destination) == 1)
	{
		// if so, increment it
		colorVectors_[color][destination] += 1;
	}
	else
	{
		// there's not, so just set to the 1
		colorVectors_[color][destination] = 1;
	}

	// if the message to be send is red,
	// we need to update the Tmin value (Mattern's algorithm)
	if (color == kRed1 || color == kRed2) {
		if (timestamp.get_x() < tmin_)
			tmin_ = timestamp.get_x();
	}
}

/**
 * \brief Notify the simulation kernel of the receiving of a message. Needed for GVT calculation.
 *
 * @param color: the color of the received message (for Mattern's algorithm)
 */
void Solver::NotifyReceive(Color color)
{
	// update the received count
	message_received_count_++;

	// check if there's already an element in the mapping
	if (colorVectors_[color].count(id_) == 1)
	{
		// if so decrement it
		colorVectors_[color][id_] -= 1;
	}
	else
	{
		// if not, set it to -1
		colorVectors_[color][id_] = -1;
	}

	// makes sure that if the kernel is waiting for a message of this color,
	// that it resumes now (Mattern's algorithm)
	vchange_cv_[color].Set();
}

/**
 * \brief Gets the ID of the solver
 *
 * @return  The ID of the solver
 */
unsigned Solver::get_id()
{
	return id_;
}

/**
 * \brief Checks if the solver should use classic devs
 *
 * @return True if the solver uses classic devs
 */
const bool Solver::get_classic_devs() const
{
	return use_classic_devs_;
}

}

