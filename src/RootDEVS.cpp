#include "RootDEVS.h"
#include "CoupledDEVS.h"
#include "AtomicDevs.h"
#include "schedulers/SchedulerML.h"
#include "schedulers/SchedulerSL.h"
#include "schedulers/SchedulerAH.h"
#include <iostream>

namespace cdevs {
/**
 * \brief Constructor
 *
 * @param components  The atomic DEVS models that are the chsildren, only those that are ran locally should be mentioned
 * @param models  All models that have to be passed to the scheduler
 * @param scheduler_type  Type of scheduler to use (string representation)
 */
RootDevs::RootDevs(std::list<std::shared_ptr<AtomicDevs> > components, std::list<std::shared_ptr<BaseDevs> > models,
        SchedulerType scheduler_type)
	: BaseDevs("ROOT model"), components_(components), models_(models), scheduler_type_(scheduler_type), direct_connected_(
	        true)
{
}

RootDevs::RootDevs()
	: BaseDevs(""), components_(), models_(), scheduler_type_(SchedulerType::kMlSchedulerType), direct_connected_()
{
}

/**
 * \brief Destructor
 */
RootDevs::~RootDevs()
{

}

/**
 * \brief Undo direct connect for this model
 */
void RootDevs::UndoDirectConnect()
{
	direct_connected_ = false;
}

/**
 * \brief Performs direct connect for this model
 *
 * @return  The direct connected component_set
 */
std::list<std::shared_ptr<BaseDevs> > RootDevs::DirectConnect()
{
	if (!direct_connected_) {
		models_ = CoupledDevs::DirectConnect(models_);
		direct_connected_ = true;
	}

	return models_;
}

/**
 * \brief Sets the scheduler of the root model
 *
 * @param scheduler_type  The scheduler type for the root model
 */
void RootDevs::set_scheduler(SchedulerType scheduler_type)
{
	scheduler_type_ = scheduler_type;
	switch (scheduler_type) {
	case SchedulerType::kMlSchedulerType:
		scheduler_ = std::make_shared<SchedulerML>(components_, 0.00001, models_.size());
		break;
	case SchedulerType::kAhSchedulerType:
		scheduler_ = std::make_shared<SchedulerAH>(components_, 0.00001, models_.size());
		break;
	case SchedulerType::kSlSchedulerType:
		scheduler_ = std::make_shared<SchedulerSL>(components_, 0.00001, models_.size());
		break;
	default:
		scheduler_ = std::make_shared<SchedulerML>(components_, 0.00001, models_.size());
		break;
	}
}

void RootDevs::set_gvt(double gvt, bool last_state_only)
{
	for (auto& component : components_) {
		component->set_GVT(gvt, last_state_only);
	}
}

bool RootDevs::Revert(DevsTime at_time)
{
	std::list<std::shared_ptr<AtomicDevs> > reschedules;
	bool controller_revert = false;
	for (auto& component : components_) {
		if (!(component->get_time_last() < at_time)) {
			controller_revert |= component->Revert(at_time);
			reschedules.push_back(component);
		}
		component->my_input_.clear();
	}
	scheduler_->MassReschedule(reschedules);
	SetTimeNext();
	return controller_revert;
}


/**
 * \brief Sets the time next of the root model, according to the scheduler
 */
void RootDevs::SetTimeNext()
{
	time_next_ = scheduler_->ReadFirst();
}

/**
 * \brief Gets a shared pointer to the root model
 *
 * @return  A shared pointer to the root model
 */
std::shared_ptr<BaseDevs> RootDevs::getPtr()
{
	return shared_from_this();
}

/**
 * Returns the component set of this DEVS element
 *
 * @return The component set
 */
std::list<std::shared_ptr<AtomicDevs> > RootDevs::get_component_set()
{
	return components_;
}

/**
 * \brief Flattens the connections
 */
void RootDevs::FlattenConnections()
{

}

/**
 * \brief Unlattens the connections
 */
void RootDevs::UnflattenConnections()
{

}

/**
 * \brief Sets the location of the model
 *
 * @param location  The location to move the model onto
 * @param force  Flag determining whether the location be forced when negative
 */
void RootDevs::set_location(int location, bool force)
{
}

/**
 * \brief Finalize a root devs, preparing it for the simulation
 *
 * @param name  The name to precede the model's name
 * @param model_counter The counter so that each model has a unique ID
 * @param model_ids  An id to model mapping
 * @param select_hierarchy  The select hierarchy
 */
int RootDevs::Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
        std::list<std::shared_ptr<BaseDevs>> select_hierarchy)
{
	return 0;
}

/**
 * Adds component to our current set
 *
 * @param component  The component to add
 */
void RootDevs::AddComponent(std::shared_ptr<AtomicDevs> component)
{
	components_.push_back(component);
}

/**
 * \brief Gets the scheduler
 *
 * @return  The scheduler of the root model
 */
Scheduler * RootDevs::get_scheduler()
{
	return scheduler_.get();
}

/**
 * \brief Gets the scheduler type
 *
 * This functions returns what type of scheduler this RootDEVS is using
 * It can be:
 * 	- Activity Heap
 * 	- Minimal List
 * 	- Sorted List
 *
 * @return The type of scheduler
 */
SchedulerType RootDevs::get_scheduler_type()
{
	return scheduler_type_;
}
} /* namespace cdevs */
