/*
 * SchedulerAH.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERAH_H_
#define SCHEDULERAH_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <memory>
#include <tuple>
#include <cmath>
#include <algorithm>
#include <limits>
#include <iostream>
#include "../exceptions/RuntimeDevsException.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs {

/**
 *     This Heap Scheduler differs a bit from Pypdevs implementation. We chose not to maintain a
 *      secondary id_fetch list, to maintain heap structures, and maintain the "dirtyness" of the heap,
 *      as this required quite some extra work for little to no performance. We can use a vector, together with
 *      std::x_heap functions to maintain a very performant heap, gaining all of the benefits of C++11 and none
 *      of the downsides of bad performance.
 */
struct Comp
{
	bool operator()(const std::tuple<DevsTime, std::shared_ptr<AtomicDevs> >& s1,
	        const std::tuple<DevsTime, std::shared_ptr<AtomicDevs> >& s2)
	{
		return std::get<0>(s1) < std::get<0>(s2);
	}
};
class SchedulerAH: public Scheduler
{
public:
	SchedulerAH(std::list<std::shared_ptr<AtomicDevs> > &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerAH();
	double get_epsilon() const;
	int get_total_models() const;

private:
	SchedulerAH();
	void PrettyPrint();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Scheduler>(this), heap_, epsilon_, totalModels_);
	}

	std::vector<std::tuple<DevsTime, std::shared_ptr<AtomicDevs> > > heap_;
	double epsilon_;
	int totalModels_;

};

} /* namespace ns_DEVS */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::SchedulerAH)

#endif /* SCHEDULERAH_H_ */
