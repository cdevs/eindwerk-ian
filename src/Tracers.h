#ifndef TRACERS_H_
#define TRACERS_H_

#include <iostream>
#include <fstream>
#include <utility>
#include <string>
#include <vector>
#include <memory>
#include "AtomicDevs.h"
#include "tracers/Tracer.h"

#include <cereal/access.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>

namespace cdevs {

/**
 * Interface for all tracers
 */
class Tracers
{
public:
	Tracers();
	virtual ~Tracers();
	void RegisterTracer(std::shared_ptr<Tracer> tracer);
	bool HasTracers() const;
	std::shared_ptr<Tracer> GetById(int uid) const;
	std::vector<std::shared_ptr<Tracer> > GetTracers() const;

	void StartTracers(bool recover);
	void StopTracers();

//	void Trace(DevsTime time, std::string text);
	template<typename T>
	void TracesUser(int time, std::shared_ptr<AtomicDevs> devs, std::string variable, T value);
	void TracesInitialize(std::shared_ptr<AtomicDevs> devs, DevsTime t);
	void TracesInternal(std::shared_ptr<AtomicDevs> devs);
	void TracesExternal(std::shared_ptr<AtomicDevs> devs);
	void TracesConfluent(std::shared_ptr<AtomicDevs> devs);
	void Reset();
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(tracers_, uid_);
	}

	std::vector<std::shared_ptr<Tracer> > tracers_;

	int uid_;
};

template<typename T>
inline void Tracers::TracesUser(int time, std::shared_ptr<AtomicDevs> devs, std::string variable, T value)
{
}
}
#endif /* TRACERS_H_ */
