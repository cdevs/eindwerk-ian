#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cfloat>
#include <thread>

#include "Color.h"
#include "Controller.h"
#include "allocators/Allocator.h"
#include "tracers/Tracer.h"
#include "exceptions/RuntimeDevsException.h"
#include "tracers/TracerVerbose.h"
#include "tracers/TracerXML.h"
#include "tracers/TracerJson.h"

#include <cereal/access.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

namespace cdevs {

/**
 * \brief Associates a hierarchical DEVS model with the simulation engine
 *
 * This is the actual interface to the simulation
 */
class Simulator
{
public:
	Simulator(std::shared_ptr<BaseDevs> model, unsigned n_kernels = 1);

	void LoadCheckpoint();

	void StartAllocator();

	void set_classic_devs();

	void set_termination_condition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> condition);
	void set_termination_time(double time);

	void set_checkpoint_name(std::string name);
	void set_checkpoint_interval(unsigned interval);
	void set_gvt_interval(unsigned interval);

	void Simulate();
	void RealSimulate();

	virtual ~Simulator();

	void set_tracer(std::shared_ptr<Tracer> tracer);
	void set_verbose(std::string filename = "");
	void set_xml(std::string filename = "");
	void set_json(std::string filename = "");
	void set_custom_tracer(std::shared_ptr<Tracer> tracer);

	void set_scheduler_list(); //Default should be list
	void set_scheduler_heap();
	void set_scheduler_sorted_list();

	static std::unique_ptr<Simulator> LoadFromCheckpoint(std::string checkpoint_name, double gvt);
	unsigned get_n_kernels() const;

private:
	void RunStartup();
	void Checkpoint();

	void set_n_kernels(unsigned kernels);

	SchedulerType sim_scheduler_type_;

	std::shared_ptr<Allocator> allocator_;
	std::shared_ptr<Controller> controller_;
	std::shared_ptr<BaseDevs> model_;
	std::map<int, std::shared_ptr<BaseDevs>> model_ids_;

	std::vector<int> termination_models_;
	std::shared_ptr<Tracers> tracers_;

	bool has_location_tracer_;
	bool is_in_progress_;

	bool is_coupled_devs_;
	bool draw_model_hierarchy_;

	bool is_flattened_;
	bool setup_;

	int gvt_interval_;
	double termination_time_;

	std::string checkpoint_name_;
	int checkpoint_interval_;

	unsigned n_kernels_;

	std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(controller_, model_, model_ids_, termination_models_, tracers_,
		 has_location_tracer_, is_in_progress_, is_coupled_devs_,
		 draw_model_hierarchy_, is_flattened_, setup_, gvt_interval_, termination_time_,
		 checkpoint_name_, checkpoint_interval_, n_kernels_);
	}
};

} /* namespace ns_DEVS */

#endif /* SIMULATOR_H_ */
