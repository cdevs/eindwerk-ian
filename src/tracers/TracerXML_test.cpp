#include "../tracers/TracerXML.h"
#include "../AtomicDevs.h"
#include "../../examples/atomicmodel_test/model.h"
#include "../exceptions/LogicDevsException.h"
#include <iostream>
#include "gtest/gtest.h"
#include "../Controller.h"
#include "atomicmodel_test/model.h"

namespace cdevs {

class TracerXmlTest: public ::testing::Test
{
protected:
	void Constructors()
	{
		std::shared_ptr<TracerXml> t1 = std::make_shared<TracerXml>(controller_, "out.xml");
		EXPECT_EQ(t1->filename_, "out.xml");
		EXPECT_FALSE(t1->stream_);

		std::shared_ptr<TracerXml> t2 = std::make_shared<TracerXml>(controller_);
		EXPECT_EQ(t2->filename_, "");
		EXPECT_FALSE(t2->stream_);

	}

	void StartStop()
	{
		std::shared_ptr<TracerXml> t = std::make_shared<TracerXml>(controller_, "out.xml");
		EXPECT_FALSE(t->stream_);
		EXPECT_THROW(t->PrintString("Error"), LogicDevsException);
		t->StartTracer(false);
		EXPECT_TRUE(t->stream_);
		t->StopTracer();
		EXPECT_FALSE(t->stream_);
		std::ifstream o("out.xml");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("<trace></trace>", out.str());
		remove("out.xml");

		t = std::make_shared<TracerXml>(controller_, "");
		//Redirect cout
		std::stringbuf* my_cout = new std::stringbuf();
		std::streambuf *old = std::cout.rdbuf(my_cout);
		EXPECT_FALSE(t->stream_);
		t->StartTracer(false);
		EXPECT_EQ(&std::cout, t->stream_);
		t->StopTracer();
		//Restore cout
		std::cout.rdbuf(old);
		EXPECT_EQ("<trace></trace>", my_cout->str());
		EXPECT_FALSE(t->stream_);
		delete my_cout;
	}

	void Print()
	{
		std::shared_ptr<TracerXml> t = std::make_shared<TracerXml>(controller_, "out.xml");
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		std::ifstream o("out.xml");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("<trace>This is a test</trace>", out.str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
		remove("out.xml");
	}

	void TraceInitialize()
	{
		std::shared_ptr<TracerXml> t = std::make_shared<TracerXml>(controller_, "out.xml");
		std::string modelpath = "examples/libAtomicModelTest";
		std::shared_ptr<AtomicDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
		model->set_time_next(DevsTime(60, 0));
		t->StartTracer(false);
		t->TraceInitialize(model, DevsTime());
		controller_->PerformActions();
		t->StopTracer();
		std::ifstream o("out.xml");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/xml/TraceInitialize.xml");
		std::stringstream exp;
		exp << e.rdbuf();
		EXPECT_EQ(exp.str(), out.str());
		remove("out.xml");
	}

	virtual void SetUp()
	{
		controller_ = Controller::create(NULL);
	}
	virtual void TearDown()
	{
	}

	std::shared_ptr<Controller> controller_;
};

TEST_F(TracerXmlTest, Constructors)
{
	Constructors();
}

TEST_F(TracerXmlTest, StartStop)
{
	StartStop();
}

TEST_F(TracerXmlTest, Print)
{
	Print();
}

TEST_F(TracerXmlTest, TraceInitialize)
{
	TraceInitialize();
}

} /* namespace ns_DEVS */
