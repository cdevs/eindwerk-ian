#ifndef SRC_TRACERS_TRACERJSON_H_
#define SRC_TRACERS_TRACERJSON_H_

#include <fstream>
#include <string>
#include <vector>
#include <mutex>

#include "../tracers/Tracer.h"
#include "../utility.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/list.hpp>

#include "../Controller.h"

namespace cdevs {

class TracerJson: public Tracer
{
	friend class TracerJsonTest;

	/*
	 * A Tracer that creqtes JSON output.
	 */
public:
	TracerJson(std::shared_ptr<Controller> controller, std::string filename="");
	virtual ~TracerJson();

	void StartTracer(bool recover);
	void StopTracer();
	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);
private:
	std::string MakeTrace(std::shared_ptr<AtomicDevs> aDEVS, bool internal);
	std::string PortsToJson(std::string port_kind,
	        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	                std::owner_less<std::weak_ptr<Port> > >& my_output_);

	TracerJson();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Tracer>(this), first_event_);
	}

	bool first_event_;
};

} /* namespace cdevs */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::TracerJson)

#endif /* SRC_TRACERS_TRACERJSON_H_ */
