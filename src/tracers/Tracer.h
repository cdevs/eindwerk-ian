#ifndef TRACER_H_
#define TRACER_H_

#include "../AtomicDevs.h"
#include "../utility.h"
#include "../Port.h"

#include <iostream>
#include <gtest/gtest_prod.h>
#include <fstream>
#include <memory>
#include <cstddef>

#include <cereal/access.hpp>

namespace cdevs {

class Controller;

class Tracer : public std::enable_shared_from_this<Tracer>
{
	/*
	 Base class for Tracer.

	 This class provides a start and stop method, a print method.
	 */
public:
	Tracer(std::string filename="");
	Tracer(std::shared_ptr<Controller> controller, std::string filename = "");
	virtual ~Tracer();
	virtual void StartTracer(bool recover);
	virtual void StopTracer();
	virtual void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t) = 0;
	virtual void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void Trace(std::shared_ptr<AtomicDevs> aDEVS, DevsTime time, std::string text) = 0;

	void PrintString(std::string text) const;

	void Flush();

	void set_controller(std::shared_ptr<Controller> controller);
protected:
	std::shared_ptr<Controller> controller_;
	std::string filename_;
	std::ostream* stream_;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(controller_, filename_);
	}

	template<class Archive>
	static void load_and_construct(Archive & ar, cereal::construct<Tracer> & construct)
	{
		std::shared_ptr<Controller> controller_;
		int uid_;
		std::string filename_;

		ar(controller_, uid_, filename_);

		construct(controller_, uid_, filename_);
	}
};

}

#endif
