#include "../tracers/TracerVerbose.h"

#include "../Port.h"
#include "../Controller.h"
#include <iostream>
#include <string>
#include <cfloat>

namespace cdevs {

TracerVerbose::TracerVerbose()

	: Tracer(), prevtime_(DevsTime(-1, -1))

{

}

TracerVerbose::TracerVerbose(std::shared_ptr<Controller> controller, std::string filename)

	: Tracer(controller, filename), prevtime_(-1, -1)

{

}
/**
 * \brief Destructor
 */
TracerVerbose::~TracerVerbose()
{
}

/**
 * \brief Performs the initialize trace for the given
 * atomic model at the given time
 *
 * @param aDevs  The model to perform the initialize trace for
 * @param t  The time of calling the initialize trace
 */
void TracerVerbose::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	std::string text = "\n";
	text += "\tINITIAL CONDITIONS in model <" + aDEVS->get_model_full_name() + ">\n";
	text += "\t\tInitial State: " + aDEVS->getState().string() + "\n";
	text += "\t\tNext scheduled internal transition at time " + aDEVS->get_time_next().string() + "\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
// Trace(aDEVS, aDEVS->getTimeLast(), text);
}

/**
 * \brief Performs the internal trace for the given
 * atomic model
 *
 * @param devs  The model to perform the internal trace for
 */
void TracerVerbose::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "\n";
	text += "\tINTERNAL TRANSITION in model <" + aDEVS->get_model_full_name() + ">\n";
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tOutput Port Configuration:\n";

	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "\t\t\tport <" + port_msgs_pair.first.lock()->get_port_name() + ">:\n";
		for (auto msg : port_msgs_pair.second) {
			text += "\t\t\t\t" + msg->string() + "\n";
		}
	}

	text += "\t\tNext scheduled internal transition at time " + aDEVS->get_time_next().string() + "\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
// Trace(aDEVS, aDEVS->getTimeLast(), text);
}

/**
 * \brief Performs the external trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the external trace for
 */
void TracerVerbose::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "\n";
	text += "\tEXTERNAL TRANSITION in model <" + aDEVS->get_model_full_name() + ">\n";
	text += "\t\tInput Port Configuration:\n";
	for (auto& port : aDEVS->get_input_ports()) {
		text += "\t\t\tport <" + port->get_port_name() + ">: \n";
		for (auto msg : aDEVS->my_input_[port]) {
			text += "\t\t\t\t" + msg->string() + "\n";
		}
	}
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tNext scheduled internal transition at time " + aDEVS->get_time_next().string() + "\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
// Trace(aDEVS, aDEVS->getTimeLast(), text);
}

/**
 * \brief Performs the confluent trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the confluent trace for
 */
void TracerVerbose::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "\n";
	text += "\tCONFLUENT TRANSITION in model <" + aDEVS->get_model_full_name() + ">\n";
	text += "\t\tInput Port Configuration:\n";
	for (auto& port : aDEVS->get_input_ports()) {
		text += "\t\t\tport <" + port->get_port_name() + ">: \n";
		for (auto msg : aDEVS->my_input_[port]) {
			text += "\t\t\t\t" + msg->string() + "\n";
		}
	}
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tOutput Port Configuration:\n";

	for (auto& port : aDEVS->get_output_ports()) {
		text += "\t\t\tport <" + port->get_port_name() + ">:\n";
		for (auto msg : aDEVS->my_output_[port]) {
			text += "\t\t\t\t" + msg->string() + "\n";
		}
	}

	text += "\t\tNext scheduled internal transition at time " + aDEVS->get_time_next().string() + "\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
// Trace(aDEVS, aDEVS->getTimeLast(), text);
}

/**
 * \brief Performs a trace for the given
 * atomic model
 *
 * @param model  The model to perform the trace for
 * @param time  The time at which the trace is invoked
 * @param text  The text to output
 */
void TracerVerbose::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	if (time > this->prevtime_) {
		text = "\n__  Current Time: " + time.string() + " " + std::string(42, '_') + " \n\n" + text;
		prevtime_ = time;
	}

	PrintString(text);
}
} /* namespace cdevs */
