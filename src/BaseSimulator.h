#ifndef BASESIMULATOR_H_
#define BASESIMULATOR_H_

#include "templates/Scheduler.h"
#include "AtomicDevs.h"
#include "MessageScheduler.h"
#include "GvtControlMessage.h"
#include "utility.h"
#include "Message.h"
#include "Tracers.h"

#include "ThreadEvent.h"
#include <array>
#include <ctime>
#include <chrono>
#include <map>
#include <list>
#include <functional>
#include <exception>
#include <queue>
#include <cfloat>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <limits>
#include <thread>
#include <mutex>
#include <memory>
#include <condition_variable>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

#include "CoupledDEVS.h"
#include "RootDEVS.h"
#include "Solver.h"
#include "ThreadEvent.h"
#include "tracers/TracerAction.h"

#define DEF_MAX_LOOPS 1000
#define DEF_EPSILON 1E-6

namespace cdevs {

class CoupledDevs;
class Controller;

/**
 * \brief The BaseSimulator class, this is the actual simulation kernel.
 */
class BaseSimulator: public Solver
{
public:
	BaseSimulator(unsigned id, std::shared_ptr<RootDevs> model);

	void ResetSimulation(Scheduler * scheduler);

	void Initialize();

	void set_termination_time(DevsTime at_time);

	void set_globals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
	        unsigned nKernels, std::shared_ptr<Tracers> tracers);

	void PrepareModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
	        bool is_flattened);
	void SendModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
	        SchedulerType scheduler_type, bool is_flattened);

	//GVT
	void set_GVT(double gvt, bool last_state_only);
	double get_GVT();

	void Revert(DevsTime at_time);

	void Receive(Message message);

	void ReceiveAntimessages(DevsTime min_time, std::shared_ptr<AtomicDevs> model, std::list<Message> messages,
	        Color color);

	void Finish();

	void MassDelayedActions(DevsTime at_time, const std::vector<TracerAction>& messages);
	void RemoveActions(std::vector<std::shared_ptr<AtomicDevs>> model_ids, DevsTime time);
	void PerformActions(double gvt = std::numeric_limits<double>::infinity());

	void RemoveTracers();

	void LoadCheckpoint();

	void SimulateSync();
	void Simulate();

	void StartTracers(bool recover);
	void StopTracers();

	StateBase const& get_state_at_time(int model_id, DevsTime request_time);

	std::string GenerateUuid();

	bool isFlattened() const;
	void set_isFlattened(bool isFlattened);

	int FinishRing(int messages_sent, int messages_received, bool first_run = false);

	virtual ~BaseSimulator();
protected:
	BaseSimulator();
private:
	void Checkpoint();

	void RunSimulation();
	bool Check();

	void ProcessIncomingMessages();
	DevsTime ProcessMessage(DevsTime clock);

	double get_time();

	bool WaitUntilOK(unsigned vector);

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Solver>(this), checkpoint_name_, next_LP_, message_input_scheduler_, control_msg_,
		        is_flattened_, has_prevtime_finished_, is_finish_sent_, has_simulation_reset_,
		        termination_time_, gvt_, old_gvt_, prev_time_, clock_, current_clock_, checkpoint_counter_,
		        checkpoint_frequency_, reverts_, actions_, loglevel_, simlock_request_, n_kernels_,
		        has_simulation_finished_, total_model_, termination_time_check_, accumulator_);
	}

	//////////////////////////
	// STRINGS
	//////////////////////////
	std::string checkpoint_name_; // checkpoint names

	std::shared_ptr<BaseSimulator> next_LP_;

	//////////////////////////
	// MESSAGES
	//////////////////////////
	MessageScheduler message_input_scheduler_;

	GvtControlMessage control_msg_;

	//////////////////////////
	// TRANSITIONING
	//////////////////////////

	//////////////////////////
	// FLAGS
	/////////////////////////
	// model
	bool is_flattened_; // is the total model flattened

	// simulation
	bool has_prevtime_finished_;

	bool is_finish_sent_;

	bool has_simulation_reset_;

	//////////////////////////
	// TIME
	//////////////////////////
	DevsTime termination_time_; // when should we stop?

	double gvt_; // the gvt to roll to
	double old_gvt_;

	DevsTime prev_time_;
	DevsTime clock_;
	DevsTime current_clock_;

	//////////////////////////
	// COUNTERS
	/////////////////////////
	int checkpoint_counter_;
	int checkpoint_frequency_;

	int reverts_;

	/////////////////////////
	// TRACERS
	/////////////////////////
	std::vector<TracerAction> actions_;

	unsigned loglevel_;

	//////////////////////////
	// MUTEX LOCKS
	//////////////////////////
	bool simlock_request_;

	std::mutex actionmutex_;

protected:
	void DelayedAction(std::weak_ptr<Tracer> tracer, DevsTime at_time, std::shared_ptr<AtomicDevs> model, std::string text);

	void ReceiveControl(GvtControlMessage message, bool first = false);

	unsigned n_kernels_;

	bool has_simulation_finished_;

	std::mutex sim_lock_;
	std::mutex v_lock_;

	ThreadEvent should_run_;
	ThreadEvent sim_finish_;

	//////////////////////////
	// MODELS
	//////////////////////////
	std::shared_ptr<BaseDevs> total_model_;

	/////////////////////////
	// CONDITION
	/////////////////////////
	bool termination_time_check_;

	//////////////////////////
	// GVT
	//////////////////////////
	std::map<unsigned, int> accumulator_;
};

inline bool BaseSimulator::isFlattened() const
{
	return is_flattened_;
}

inline void BaseSimulator::set_isFlattened(bool isFlattened)
{
	is_flattened_ = isFlattened;
}

} /* namespace ns_DEVS */

#endif /* BASESIMULATOR_H_ */
