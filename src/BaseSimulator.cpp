#include "BaseSimulator.h"
#include "Controller.h"
#include "exceptions/LogicDevsException.h"
#include "exceptions/RuntimeDevsException.h"
#include <iomanip>
#include <limits>
#include <set>
#include <cereal/archives/binary.hpp>

namespace cdevs {
/**
 * \brief Constructor
 *
 * @param id  The id of the kernel
 * @param model  The RootDevs model to simulate
 */
BaseSimulator::BaseSimulator(unsigned id, std::shared_ptr<RootDevs> model)
	: Solver(id)
{
	Initialize();

	checkpoint_name_ = "(none)";

	model_ = model;
	total_model_ = 0;

	is_flattened_ = false;

	is_simulation_irreversible_ = true;
	has_simulation_finished_ = false;
	has_prevtime_finished_ = false;

	termination_time_check_ = true;
	is_finish_sent_ = false;

	has_simulation_reset_ = false;

	simlock_request_ = false;

	checkpoint_counter_ = 0;
	checkpoint_frequency_ = 5;

	gvt_ = -std::numeric_limits<double>::infinity();
	old_gvt_ = 0.0;

	reverts_ = 0;
}

/**
 * \brief Default constructor (private, for serializing)
 */
BaseSimulator::BaseSimulator()
	: is_flattened_(), has_prevtime_finished_(), is_finish_sent_(), has_simulation_reset_(),
	  termination_time_(), gvt_(), old_gvt_(), checkpoint_counter_(), checkpoint_frequency_(),
	  reverts_(), loglevel_(), simlock_request_(), n_kernels_(), has_simulation_finished_(),
	  termination_time_check_()
{
}

/**
 * Resets the simulation kernel to the saved version; can only be invoked after a previous simulation run.
 *
 * @param scheduler: the scheduler to set
 */
void BaseSimulator::ResetSimulation(Scheduler * scheduler)
{
	// recall all initialisations
	Initialize();

	// reset model transitions
	transitioning_.clear();

	//reset tracers
	tracers_->Reset();

	// reset flags
	is_simulation_irreversible_ = false;
	checkpoint_restored_ = false;
	has_simulation_reset_ = true;
}

/**
 * \brief Initialise the simulation kernel, this is split up from the constructor to
 * make it possible to reset the kernel without reconstructing the kernel.
 */
void BaseSimulator::Initialize()
{
	models_.clear();

	termination_time_ = DevsTime();

	has_prevtime_finished_ = false;

	gvt_ = -std::numeric_limits<double>::infinity();
	tmin_ = std::numeric_limits<double>::infinity();
	prev_time_.set_x(0);
	prev_time_.set_y(0);
	clock_.set_x(0);
	clock_.set_y(0);

	input_message_queue_.clear();
	output_message_queue_.clear();

	message_input_scheduler_ = MessageScheduler();

	colorVectors_ = { {}, {}, {}, {}};
}

/**
 * \brief Configure all 'global' variables for this kernel.
 *
 * @param loglevel Level of logging library.
 * @param checkpoint_frequency Frequency at which checkpoints should be made.
 * @param checkpoint_name Name of the checkpoint to save.
 * @param nKernels  The amount of kernels
 * @param tracers  The tracers to use
 */
void BaseSimulator::set_globals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
        unsigned nKernels, std::shared_ptr<Tracers> tracers)
{
	loglevel_ = loglevel;
	n_kernels_ = nKernels;
	next_LP_ = controller_.lock()->get_kernel((id_ + 1) % nKernels);
	is_simulation_irreversible_ = false;
	temporarily_irreversible_ = is_simulation_irreversible_;
	// TODO: state saving
	// TODO: set logger
	checkpoint_name_ = checkpoint_name;
	checkpoint_frequency_ = checkpoint_frequency;
	checkpoint_counter_ = 0;

	tracers_ = tracers;
	do_some_tracing_ = tracers_->HasTracers();
}

/**
 * \brief Sets the time at which simulation should stop, setting this will override the local simulation condition.
 *
 * @param time the time at which the simulation should stop
 */
void BaseSimulator::set_termination_time(DevsTime at_time)
{
	termination_time_ = at_time;

	// set it in case the kernel was already stopped and an invalidation happened
	should_run_.Set();
}

/**
 * \brief Prepare a model to send
 *
 * @param model: the model to send
 * @param model_ids: list contain all models
 * @param is_flattened: whether or not the model had its ports decoupled from the models to allow pickling
 */
void BaseSimulator::PrepareModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
        bool is_flattened)
{
	is_flattened_ = is_flattened;

	if (is_flattened)
		model->UnflattenConnections();

	total_model_ = model;
	models_ = models;
}

/**
 * \brief Send a model to this simulation kernel, as this one will be simulated
 *
 * @param model  the model to set
 * @param models  list containing all models
 * @param scheduler_type  string representation of the scheduler to use
 * @param is_flattened  whether or not the model had its ports decoupled from the models to allow pickling
 */
void BaseSimulator::SendModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models,
        SchedulerType scheduler_type, bool is_flattened)
{

	//prepare the model
	PrepareModel(model, models, is_flattened);

	// find models local to this kernel
	std::list<std::shared_ptr<AtomicDevs>> local;
	for (auto& a_devs : models) {
		if (a_devs->get_location() == id_)
			local.push_back(a_devs);
	}

	// find the base models
	std::list<std::shared_ptr<BaseDevs>> base_models;
	std::shared_ptr<CoupledDevs> coupled = std::dynamic_pointer_cast<CoupledDevs>(model);
	if (coupled)
		base_models = coupled->GetComponentSet();
	else
		base_models.push_back(model);

	model_ = std::make_shared<RootDevs>(local, base_models, scheduler_type);

	// Could still be useful to test the auto allocator:
//	for (auto comp : model_->GetComponentSet())
//		std::cout << "\t" << comp->GetModelName() << " : " << comp->GetLocation() << std::endl;
}

/**
 * Sets the GVT of this simulation kernel. This value should not be smaller than
 * the current GVT (this would be impossible for a correct GVT calculation). Also
 * cleans up the input, output and state buffers used due to time-warp.
 * Furthermore, it also processes all messages scheduled before the GVT.
 *
 * @param gvt: the desired GVT
 * @param activities: the activities of all seperate nodes as a list
 * @param last_state_only: whether or not all states should be considered or only the last
 */
void BaseSimulator::set_GVT(double gvt, bool last_state_only)
{
	// check if GVT is higher than the current, if not, an exception should be thrown
	if (gvt < gvt_)
		throw RuntimeDevsException("BaseSimulator trying to set gvt lower than original");
	else if (gvt == gvt_) {
		// the gvt for this kernel is already set properly, so we're done
		controller_.lock()->GvtDone();
		return;
	}

	// this is sleep-looped in the RunSimulation, to give priority to the setting of GVT
	simlock_request_ = true;

	// request access
	sim_lock_.lock();

	// set flag to false already, so the RunSimulation can occur as soon as we unlock
	simlock_request_ = false;

	// store the gvt
	old_gvt_ = gvt_;
	gvt_ = gvt;

	// perform actions up until the GVT
	PerformActions(gvt);

	// perform fossil collection
	// a flag to check if we found a message beyond the GVT
	bool found = false;

	// clean up the input scheduler
	message_input_scheduler_.CleanUp(DevsTime(gvt, 1));

	// check if we can find an output message newer than the GVT, if so, erase everything up until this message
	for (unsigned i = 0; i < output_message_queue_.size(); i++) {
		if (output_message_queue_.at(i).get_timestamp().get_x() >= gvt) {
			found = true;
			output_message_queue_.erase(output_message_queue_.begin(), output_message_queue_.begin() + i);
			break;
		}
	}

	// the entire output message queue is old, so clear the entire queue
	if (!found)
		output_message_queue_.clear();

	// set the model GVT
	model_->set_gvt(gvt, last_state_only);

	// make sure we perform a checkpoint at the appriopriate time intervals
	if ((checkpoint_frequency_ > 0) && (checkpoint_counter_ == checkpoint_frequency_)) {
		Checkpoint();
		checkpoint_counter_ = 0;
	} else
		checkpoint_counter_ += 1;

	// release the simulation lock
	sim_lock_.unlock();

	// let the next kernel set its GVT
	next_LP_->set_GVT(gvt, last_state_only);
}

/**
 * \brief Return the GVT of this kernel
 *
 * @return The current GVT
 */
double BaseSimulator::get_GVT()
{
	return gvt_;
}

/**
 *  \brief Revert the current simulation kernel to the specified time. All messages
 *  sent after this time will be invalidated, all states produced after this
 *  time will be removed.
 *
 *  Note: Clearly, this time should be >= the current GVT
 *
 *  @param time the desired time for revertion.
 *
 */
void BaseSimulator::Revert(DevsTime time)
{
	// check if the revert time is valid
	if (time.get_x() < gvt_)
		throw RuntimeDevsException("BaseSimulator::Revert We cannot revert to a value smaller than the GVT.");

	// clear the model transitions
	transitioning_.clear();

	// count the amount of reverts
	reverts_ += 1;

	// remove actions given by our local models up until this time
	if (do_some_tracing_) {
		std::vector<std::shared_ptr<AtomicDevs>> locals;
		for (std::shared_ptr<AtomicDevs> model : models_) {
			locals.push_back(model);
		}
		controller_.lock()->RemoveActions(locals, time);
	}

	// Also revert the input message scheduler
	message_input_scheduler_.Revert(time);

	// Now revert all local models
	bool controller_revert = model_->Revert(time);

	// store the time
	prev_time_ = time;
	clock_ = prev_time_;

	// invalidate all output messages after or at time
	auto end = output_message_queue_.end();

	// update the unschedule messages
	std::map<std::shared_ptr<AtomicDevs>, std::list<Message> > unschedules;
	std::map<std::shared_ptr<AtomicDevs>, DevsTime> unschedules_min_time;

	for (Message& message : output_message_queue_) {
		if (message.get_timestamp() > time) {
			int model_id = message.get_destination();
			std::shared_ptr<AtomicDevs> model = FindDevsModelById(model_id);

			// set default min_time
			DevsTime min_time(std::numeric_limits<double>::infinity(), 0);

			// check if key exists, if so, update the min_time
			if (unschedules_min_time.find(model) != unschedules_min_time.end())
				min_time = unschedules_min_time[model];

			// set the min_time for the model equal to the minimum of the min_time and the message timestamp
			if (min_time < message.get_timestamp())
				unschedules_min_time[model] = min_time;
			else
				unschedules_min_time[model] = message.get_timestamp();

			// if we can find the key for this model, add it to the unschedules
			unschedules[model].push_back(message);

		} else
			end = std::find(output_message_queue_.begin(), output_message_queue_.end(), message);
	}

	// clean up the output message queue
	if (end != output_message_queue_.end())
		output_message_queue_.erase(end + 1, output_message_queue_.end());

	// make sure that we prevent improper messages, for this given time, from being sent
	// [ see Solver::Send() ]
	if (output_message_queue_.empty())
		block_outgoing_ = DevsTime(-1, -1);
	else
		block_outgoing_ = output_message_queue_.back().get_timestamp();

	for (auto model_messages_pair : unschedules) {
		// get the model
		std::shared_ptr<AtomicDevs> model = model_messages_pair.first;

		// get the model's destination
		int destination_kernel = model->get_location();

		// revert to relocation itself... This should be impossible
		if (destination_kernel == id_)
			throw RuntimeDevsException("Revert due to relocation to self... This is impossible");

		// get the min time for this model
		DevsTime min_time = unschedules_min_time[model];

		// notify the sending of a message
		NotifySend(destination_kernel, min_time, color_);

		// call the destination kernel to receive antimessages to be unscheduled
		controller_.lock()->get_kernel(destination_kernel)->ReceiveAntimessages(min_time, model,
		        unschedules[model], color_);

		if (controller_revert) {
			NotifySend(controller_.lock()->get_id(), time, color_);
			controller_.lock()->ReceiveAntimessages(time, 0, { }, color_);
		}
		should_run_.Set();
	}
}

/**
 * \brief Receives a message and makes sure that the simulation runs again
 *
 * @param message  The message that is received
 */
void BaseSimulator::Receive(Message message)
{
	// push the message in the queue
	input_message_queue_.push_back(message);

	// tell the kernel that it should stop waiting
	should_run_.Set();
}

/**
 * \brief Process all incomming messages and return.
 *
 * This is part of the main simulation loop instead of being part of the message receive method,
 * as we require the simlock for this. Acquiring the simlock elsewhere might take some time!
 *
 */
void BaseSimulator::ProcessIncomingMessages()
{
	while (!input_message_queue_.empty()) {
		Message message = input_message_queue_.front();
		input_message_queue_.pop_front();

		// look up destination model
		std::shared_ptr<AtomicDevs> destination_model = FindDevsModelById(message.get_destination());

		// null pointer for model
		if (destination_model == nullptr)
			throw RuntimeDevsException(
			        "BaseSimulator::ProcessIncomingMessages() has a null pointer for a message");

		// if the model is not for this kernel, send it to the right kernel
		if (destination_model->get_location() != id_) {
			// notify that we have received a message from the given color (see Mattern's)
			NotifyReceive(message.get_color());

			// now set the color of the message to what color our kernel is currently in
			message.set_color(color_);

			// send the message
			NotifySend(destination_model->get_location(), message.get_timestamp(), color_);
			controller_.lock()->get_kernel(destination_model->get_location())->Receive(message);

			// we're done with this message, check for the next message
			continue;
		}

		// check if we have to revert based on the message's timestamp
		if (message.get_timestamp() <= prev_time_)
			Revert(message.get_timestamp());
		else if (has_prevtime_finished_)
			prev_time_ = message.get_timestamp();

		// the simulation cycle is not finished yet
		has_prevtime_finished_ = false;

		// notify that we have received a message of the given color
		NotifyReceive(message.get_color());
		// schedule the message in our input
		message_input_scheduler_.Schedule(message);

		// update the time next of the model to the minimum of the received message
		// and the originally planned time next
		DevsTime min_time;

		if (model_->get_time_next() < message.get_timestamp())
			min_time = model_->get_time_next();
		else
			min_time = message.get_timestamp();

		model_->set_time_next(min_time);
	}
}

/**
 * \brief Process a (possibly huge) batch of anti messages for the same model
 *
 * @param mintime: the lowest timestamp of all messages being cancelled
 * @param model: the receiving model whose messages need to be negated, None to indicate a general rollback
 * @param messages: list of all messages to cancel
 * @param color: color for Mattern's algorithm
 *
 * note:: the *model* is only required to check whether or not the model is still local to us
 */
void BaseSimulator::ReceiveAntimessages(DevsTime min_time, std::shared_ptr<AtomicDevs> model,
        std::list<Message> messages, Color color)
{
	// request the simulation lock
	sim_lock_.lock();

	// request the color vector lock
	v_lock_.lock();

	// get location of the model
	auto destination = model->get_location();

	if (destination != id_) {
		// the model is not ours, send it the right kernel
		NotifyReceive(color);
		controller_.lock()->get_kernel(destination)->ReceiveAntimessages(min_time, model, messages, color);
		NotifySend(destination, min_time, color);

		// we're done, release locks
		v_lock_.unlock();
		sim_lock_.unlock();
		return;
	}

	// timestamp is before previous time, so revert to the time
	if (min_time <= prev_time_)
		Revert(min_time);
	// previous time is irrelevant, simulation has finished
	else if (has_prevtime_finished_)
		prev_time_ = min_time;

	// we are not finished yet with current simulation cycle
	has_prevtime_finished_ = false;
	NotifyReceive(color);

	// unschedule the messages for which we received anti messages
	if (model != nullptr)
		message_input_scheduler_.MassUnschedule(messages);

	// release the locks
	v_lock_.unlock();
	sim_lock_.unlock();
}

/**
 * \brief Checks whether or not simulation should still continue. This will either
 * call the global time termination check, or the local state termination
 * check, depending on configuration.
 * Using the global time termination check is a lot FASTER and should be used
 * if possible.
 *
 * @return Whether or not to stop simulation
 */
bool BaseSimulator::Check()
{
	// Max loops check
	if (prev_time_.get_y() > DEF_MAX_LOOPS)
		throw RuntimeDevsException("Maximal number of 0 timeAdvance loops detected");

	// check if we have to check for termination based on time
	if (termination_time_check_) {
		// check the model's time next is beyond that of the given termination time
		if (model_->get_time_next() > termination_time_) {
			// no more messages and time next is beyond termination time, so we have to stop the simulation cycle
			if (message_input_scheduler_.isEmpty())
				return true;

			// we still have messages, so check if the first one to come is beyond the termination time
			// if so, we are done, else, we're not done
			Message message = message_input_scheduler_.ReadFirst();

			DevsTime timestamp = message.get_timestamp();
			return timestamp > termination_time_;

		} else {
			// the model's time next is before the termination time, we should continue the simulation
			return false;
		}
	} else {
		// check if the prev time was set to infinity or if the termination condition is fulfilled
		if (prev_time_.get_x() == std::numeric_limits<double>::infinity()
		        || controller_.lock()->CheckTerminationCondition(prev_time_, total_model_)) {
			// check if we already sent the controller to finish at the appropriate time
			if (!is_finish_sent_) {
				// send the controller to finish at time given
				controller_.lock()->FinishAtTime(DevsTime(prev_time_.get_x(), prev_time_.get_y() + 1));
				is_finish_sent_ = true;
			}

			// we're done simulating
			return true;
		} else if (is_finish_sent_) {
			// we already requested to finish, we try this the other way
			DevsTime time;

			time.set_x(std::numeric_limits<double>::infinity());
			time.set_y(std::numeric_limits<double>::infinity());

			controller_.lock()->FinishAtTime(time);
			is_finish_sent_ = false;
		}

		// we should continue simulation
		return false;
	}

	// default answer is: continue the simulation
	return false;
}

/**
 * \brief Finishes the simulation
 */
void BaseSimulator::Finish()
{
	// request simulation lock
	sim_lock_.lock();

	// shut down all threads on the topmost simulator
	has_simulation_finished_ = true;

	// the kernel should still run and therefore stop waiting, if it was waiting
	should_run_.Set();

	// wait until they are done
	sim_finish_.Wait();

	// release the lock
	sim_lock_.unlock();
}

/**
 * \brief Call the delayedAction function multiple times in succession.
 *
 * Mainly implemented to reduce the number of round trips when tracing.
 *
 * @param at_time The time at which the action should happen
 * @param messages list containing elements of the form (model_id, action)
 */
void BaseSimulator::MassDelayedActions(DevsTime at_time, const std::vector<TracerAction>& messages)
{
	// loop the messages and perform the actions ( = performing the traces )
	for (auto action : messages) {
		DelayedAction(action.getTracer(), at_time, action.getModel(), action.getText());
	}
}

/**
 * \brief Perform an irreversible action (I/O, prints, global messages, ...). All
 * these actions will be performed in the order they should be generated
 * in a non-distributed simulation. All these messages might be reverted in
 * case a revertion is performed by the calling model.
 *
 * @param tracer  The tracer to send the text to
 * @param at_time  The simulation time at which this command was requested
 * @param model  The model that requested this command
 * @param text  The actual text to be trace as soon as it is safe
 */
void BaseSimulator::DelayedAction(std::weak_ptr<Tracer> tracer, DevsTime at_time, std::shared_ptr<AtomicDevs> model,
        std::string text)
{
	// check if we're attempting to execute actions before the given GVT, this may not happen
	if (at_time.get_x() < gvt_)
		throw RuntimeDevsException(
		        "BaseSimulator::DelayedAction Can't execute action " + text + " before GVT "
		                + std::to_string(gvt_));

	// if all actions are within a valid timespan, perform the actions
	if (is_simulation_irreversible_ && actions_.size() > 0 && at_time > actions_.back().getTime())
		PerformActions();

	// add the action to all our actions
	actionmutex_.lock();
	actions_.push_back(TracerAction(at_time, model, text, tracer));
	actionmutex_.unlock();

}

/**
 * \brief Remove all actions specified by a model, starting from a specified time.
 * This function should be called when the model is reverted and its actions
 * have to be undone
 *
 * @param model_ids The model_ids of all reverted models
 * @param at_time  Time up to which to remove all actions
 */
void BaseSimulator::RemoveActions(std::vector<std::shared_ptr<AtomicDevs>> models, DevsTime at_time)
{
	// check if we're attempting to remove actions before the given GVT, this may not happen
	if (at_time.get_x() < gvt_)
		throw RuntimeDevsException("BaseSimulator::RemoveActions Trying to remove actions before GVT");

	// make sure that we can remove the actions safely
	// lock the action mutex
	actionmutex_.lock();

	// make a list of the actions that are still to be executed
	std::vector<TracerAction> current_actions;

	// actions are unsorted, so we have to go through the complete list
	for (auto& action : actions_) {
		std::shared_ptr<AtomicDevs> model = action.getModel();
		if (std::find(models.begin(), models.end(), model) == models.end() && action.getTime() >= at_time) {
			current_actions.push_back(action);
		}
	}

	// add the updated actions
	actions_ = current_actions;

	// release the mutex
	actionmutex_.unlock();
}

/**
 * Perform all irreversible actions up to the provided time.
 * If time is not specified, all queued actions will be executed (in case simulation is finished)
 *
 * @param gvt: the time up to which all actions should be executed
 */
void BaseSimulator::PerformActions(double gvt)
{
	// check if we're attempting to perform actions before the given GVT, this may not happen
	if (gvt >= termination_time_.get_x() && controller_.lock()->get_has_termination_condition())
		gvt = termination_time_.get_x() + std::numeric_limits<double>::epsilon();

	// make sure that we can remove the actions safely
	// lock the action mutex
	actionmutex_.lock();

	// keep track of the actions to perform and the actions which remain
	std::vector<TracerAction> lst;
	std::vector<TracerAction> my_remainder;

	// if the GVT isn't infite, we need to filter the actions
	if (gvt != std::numeric_limits<double>::infinity()) {
		for (auto& action : actions_) {
			if (action.getTime().get_x() < gvt)
				lst.push_back(action);
			else
				my_remainder.push_back(action);
		}
	} else {
		// the GVT is infinite, so the whole vector of actions need to be performed
		lst = actions_;
		my_remainder.clear();
	}

	// update the actions so that only the actions which will still be unperformed remain
	actions_ = my_remainder;

	// release the lock
	actionmutex_.unlock();

	// sort the to be perfomed actions based on their time
	std::sort(lst.begin(), lst.end(),
	        [] (const TracerAction& first, const TracerAction& second)
	        { return (first.getTime() < second.getTime()) ||
	        	(first.getTime() == second.getTime()
	        		&& first.getModel()->get_model_id() < second.getModel()->get_model_id());
	        });

	std::set<std::weak_ptr<Tracer>, std::owner_less<std::weak_ptr<Tracer> > > tracers;

	// let the tracers perform the traces for the given actions
	for (auto& action : lst) {
		action.getTracer().lock()->Trace(action.getModel(), action.getTime(), action.getText());
		tracers.insert(action.getTracer());
	}

	// flush all tracers now
	// could be that we need to wait with this until no rollback can possibly occur
	for (auto& tracer : tracers)
		tracer.lock()->Flush();
}

/**
 *  \brief Removes all currently registered tracers.
 *  This does not clean them up, as this should already be done by the code at the end of the simulation.
 */
void BaseSimulator::RemoveTracers()
{
	tracers_->Reset();
}

/**
 * \brief Find the first external message smaller than the clock and process them if necessary.
 * Return the new time_next for simulation.
 *
 * @param clock timestamp of the next internal transition
 * @return timestamp of the next transition, taking into account external messages
 */
DevsTime BaseSimulator::ProcessMessage(DevsTime clock)
{
	// There are no input messages, so just return the current clock value
	if (message_input_scheduler_.isEmpty())
		return clock;

	// read the first message
	Message message = message_input_scheduler_.ReadFirst();

	if (message.get_timestamp() < clock) {
		// The message is sent before the timenext, so update the clock
		clock = message.get_timestamp();
	}

	// check if there are messages that are close enough to the current clock value
	// these will add a transition (which is external) for the given model
	while (!message_input_scheduler_.isEmpty()
	        && std::abs(clock.get_x() - message.get_timestamp().get_x()) < std::numeric_limits<double>::epsilon()
	        && clock.get_y() == message.get_timestamp().get_y()) {

		for (auto port_content_pair : message.get_content()) {
			// get the host model of the port for which the message has content
			std::shared_ptr<AtomicDevs> adevs = std::dynamic_pointer_cast<AtomicDevs>(
			        port_content_pair.first.lock()->get_host_devs().lock());

			// update the input of the model
			auto& input_now = adevs->my_input_[port_content_pair.first];
			input_now.insert(input_now.begin(), port_content_pair.second.begin(),
			        port_content_pair.second.end());

			// add an external transition
			transitioning_[adevs->get_model_id()] |= 2;

		}

		// remove the message
		message_input_scheduler_.RemoveFirst();

		// input scheduler is empty, so stop here
		if (message_input_scheduler_.isEmpty())
			break;

		// get the next message
		message = message_input_scheduler_.ReadFirst();
	}

	// return the updated clock value
	return clock;
}

/**
 * Save a checkpoint of the current basesimulator, this function will assume
 * that no messages are still left in the medium, since these are obviously
 * not saved by pickling the base simulator.
 */
void BaseSimulator::Checkpoint()
{
	// form file name string
	std::stringstream ss;
	ss << checkpoint_name_ << "_";
	ss << std::fixed;
	ss << std::setprecision(2);
	ss << std::floor(gvt_ / 100. + 0.5) * 100. << "_";
	ss << std::to_string(id_) << ".pdc";

	std::string full_checkpoint_name = ss.str();

	// create file stream
	std::ofstream dump_stream(full_checkpoint_name);

	// flatten the model for proper serialization
	if (is_flattened_)
		model_->FlattenConnections();

	{
		cereal::BinaryOutputArchive output(dump_stream);
		output(*this);
	}

	// flattened model should be unflattened
	if (is_flattened_)
		model_->UnflattenConnections();

	dump_stream.close();
}

/**
 * Alert this kernel that it is restoring from a checkpoint
 */
void BaseSimulator::LoadCheckpoint()
{
	// reset fields
	color_ = Color::kWhite1;
	transitioning_.clear();
	colorVectors_ = { {}, {}, {}, {}};
	tmin_ = std::numeric_limits<double>::infinity();
	control_msg_ = GvtControlMessage();
	// TODO: self.waiting = 0
	checkpoint_restored_ = true;

	// reset globals
	set_globals(loglevel_, checkpoint_frequency_, checkpoint_name_, n_kernels_, tracers_);

	// unflatten the model if it was flattened
	if (is_flattened_)
		model_->UnflattenConnections();

	// reset the message counters
	message_sent_count_ = 0;
	message_received_count_ = 0;

	// reset the queues
	input_message_queue_ = std::deque<Message>();
	output_message_queue_.clear();

	// reset the message scheduler
	message_input_scheduler_ = MessageScheduler();

	// reset the actions
	actions_.clear();

	// request the v lock
	v_lock_.lock();
	// revert to the GVT
	Revert(DevsTime(gvt_, 0));
	v_lock_.unlock();
}

/**
 * \brief Go over the ring and ask each kernel whether it is OK to stop simulation
 * or not. Uses a count to check that no messages are yet to be processed.
 *
 * @param messages_sent Current counter for total amount of sent messages
 * @param messages_received Current counter for total amount of received messages
 * @param first_run Whether or not to forward at the controller
 * @return Amount of messages received and sent (-1 signals running simulation)
 */
int BaseSimulator::FinishRing(int messages_sent, int messages_received, bool first_run)
{
	// check if this kernel is still running, if it is, we should not stop
	if (should_run_.isSet()) {
		return -1;
	}

	// check if we are the controller and if this is the first pass of finish ring
	if (id_ == 0 && !first_run) {
		// we are done, so return the amount of message we sent and received if they are equal
		if (messages_sent == messages_received)
			return messages_sent;

		// we should not stop
		return -1;
	}

	// if this kernel is not the controller and we are allowed to finish, ask the next kernel in the ring
	return next_LP_->FinishRing(messages_sent + message_sent_count_, messages_received + message_received_count_);

}

/**
 * \brief A small wrapper around the simulate() function
 */
void BaseSimulator::SimulateSync()
{
	Simulate();
}

/**
 * Simulate
 */
void BaseSimulator::Simulate()
{
	// TODO: check if first time this simulate function runs? - Ian
	// We already had the lock, so normal simulation
	// Send the init message
	if (gvt_ == -std::numeric_limits<double>::infinity()) {
		// To make sure that the GVT algorithm won't start already and see that this
		// model has nothing to simulate

		DevsTime new_time_next;
		model_->set_time_next(new_time_next);
		CoupledInitialize();
	}

	// set the flag to determine whether simulation is finished
	has_simulation_finished_ = false;

	// main simulation loop
	while (true) {
		// perform the actual simulating
		RunSimulation();

		// if the simulation is irreversible, we should stop right away
		if (is_simulation_irreversible_) {
			should_run_.Clear();
			break;
		}

		// if the simulation is reversible, we have to wait here
		// maybe another kernel needs this kernel to wake up
		should_run_.Wait();
		should_run_.Clear();

		// we can stop if the simulation is finished
		if (has_simulation_finished_)
			break;

	}

	// we are finished (see Finish method)
	sim_finish_.Set();
}

/**
 * \brief Run a complete simulation run.
 * Can be run multiple times
 */
void BaseSimulator::RunSimulation()
{
	while (true) {
		// check if we have messages to process
		if (!input_message_queue_.empty()) {
			sim_lock_.lock();
			v_lock_.lock();
			ProcessIncomingMessages();
			v_lock_.unlock();
			sim_lock_.unlock();
		}

		// if GVT is request the simlock, we will wait before also requesting the simlock
		while (simlock_request_) {
			std::this_thread::sleep_for(std::chrono::microseconds(10));
		}

		// request the simlock
		sim_lock_.lock();

		// check if we can stop the simulation cycle
		if (Check()) {
			// the simulation cycle has finished
			has_prevtime_finished_ = true;

			// release the lock
			sim_lock_.unlock();
			break;
		}

		// determine the time next of the model we are simulating
		// based on the message and the time next of the simulation
		// if there are no messages ProcessMessage will return
		// the model's time next
		DevsTime time_next;
		if (!is_simulation_irreversible_)
			time_next = ProcessMessage(model_->get_time_next());
		else
			time_next = model_->get_time_next();

		// if the time next is infinity, we should stop
		if (time_next.get_x() == std::numeric_limits<double>::infinity()) {
			// clear remaining model transitions
			transitioning_.clear();

			// the simulation cycle has finished
			has_prevtime_finished_ = true;

			// release the lock
			sim_lock_.unlock();
			break;
		}

		// round of the current clock time, which is necessary for revertions later on
		current_clock_.set_x(time_next.get_x());
		current_clock_.set_y(time_next.get_y());

		// check which models are to be rescheduled
		std::list<std::shared_ptr<AtomicDevs>> reschedule;
		if (use_classic_devs_) {
			v_lock_.lock();
			reschedule = CoupledOutputGenerationClassic(current_clock_);
			v_lock_.unlock();
		} else {
			v_lock_.lock();
			reschedule = CoupledOutputGeneration(current_clock_);
			v_lock_.unlock();
		}

		// perform the transitions
		MassAtomicTransitions(transitioning_, current_clock_);

		// reschedule the appropriate models
		model_->get_scheduler()->MassReschedule(reschedule);

		// request the V color lock
		v_lock_.lock();

		// set the time next depending on the scheduler's first time
		model_->SetTimeNext();

		// clear all model transitions
		transitioning_.clear();

		// check if we need to block messages we are going to send
		// [ see Solver::Send() ]
		if (!output_message_queue_.empty())
			block_outgoing_ = output_message_queue_.back().get_timestamp();
		else
			block_outgoing_ = DevsTime(-1, -1);

		// previous time is the current time
		prev_time_ = current_clock_;
		// updat the clock
		clock_ = model_->get_time_next();

		// release the locks
		v_lock_.unlock();
		sim_lock_.unlock();
	}
}

/**
 * Start all tracers
 */
void BaseSimulator::StartTracers(bool recover)
{
	tracers_->StartTracers(recover);
}

/**
 * Stop all tracers
 */
void BaseSimulator::StopTracers()
{
	tracers_->StopTracers();
}

/**
 * Return the current time of this kernel
 *
 * @return the current simulation time
 */
double BaseSimulator::get_time()
{
	return prev_time_.get_x();
}

/**
 * \brief Gets the state of a model at a specific time
 *
 * @param model_id: model_id of which the state should be fetched
 * @param request_time: time of the state
 */
StateBase const& BaseSimulator::get_state_at_time(int model_id, DevsTime request_time)
{
	return FindDevsModelById(model_id)->getState(request_time, false);
}

/**
 * Create a unique enough ID for a message
 *
 * @return a unique string for the specific name and number of sent messages
 */
std::string BaseSimulator::GenerateUuid()
{
	std::stringstream ss;
	message_sent_count_ += 1;

	ss << id_ << "-" << message_sent_count_;

	return ss.str();
}

/**
 * \brief Returns as soon as all messages to this simulation kernel are received.
 * Needed due to Mattern's algorithm. Uses events to prevent busy looping.
 *
 * @param vector The vector number to wait for. Should be 0 for colors kWhite1
 * and kRed1, should be 1 for kWhite2 and kRed2.
 * @return False when done.
 */
bool BaseSimulator::WaitUntilOK(unsigned vector)
{
	// keep a reference to the color vector (as shortcut)
	std::map<unsigned, int>& v = colorVectors_[(Color) vector];

	// check the total messages we sent
	int sent = (v.find(id_) == v.end()) ? 0 : v.at(id_);
	// check the total messages others received
	int received =
	        (control_msg_.get_wait_vector().find(id_) == control_msg_.get_wait_vector().end()) ?
	                0 : control_msg_.get_wait_vector().at(id_);

	// as long as they are not equal, there are still transient messages
	while (sent + received > 0) {
		// unlock the V color lock
		v_lock_.unlock();

		// wait until the event for the given color is set
		vchange_cv_.at(vector).Wait();
		vchange_cv_.at(vector).Clear();

		// re check sent and received
		sent = (v.find(id_) == v.end()) ? 0 : v.at(id_);
		received =
		        (control_msg_.get_wait_vector().find(id_) == control_msg_.get_wait_vector().end()) ?
		                0 : control_msg_.get_wait_vector().at(id_);

		// re lock the V color lock
		v_lock_.lock();
	}
	return false;
}

/**
 * Receive a GVT control message and process it. Method will block until the GVT is actually found, so make this an asynchronous call, or run it on a seperate thread.
 * This code implements Mattern's algorithm with a slight modification: it uses 4 different colours to distinguish two subsequent runs. Furthermore, it always requires 2 complete passes before a GVT is found.
 *
 * @param message The GVT control message
 * @param first Flag determining whether this is the first pass
 */
void BaseSimulator::ReceiveControl(GvtControlMessage message, bool first)
{
	// update the current received control message
	control_msg_ = message;

	// retrieve the fields
	double m_clock = control_msg_.get_clock();
	double m_send = control_msg_.get_send();
	std::map<unsigned, int> waiting_vector = control_msg_.get_wait_vector();
	std::map<unsigned, int> accumulating_vector = control_msg_.get_accumulate_vector();

	// lock the V color lock
	v_lock_.lock();

	// calculate next color
	Color prevcolor = (color_ == Color::kWhite1) ? Color::kRed2 : static_cast<Color>(color_ - 1);
	// keep local current color
	Color color = color_;

	// if this is the controller and at least two passes have been made and the kernel is in the white color
	// phase, then we are finished
	bool finished = (!id_ && !first && (color == Color::kWhite1 || color == Color::kWhite2));

	// a GVT bug occurs when one of the kernels did not wait until the message sent equals the messages received
	if (id_ == 0 && !first) {
		bool bug = false;
		for (auto& pair : waiting_vector) {
			if (pair.second != 0) {
				// there is a non-zero value for the given kernel, this means there are still transient
				// messages, but this is not possible in this phase, so we have detected a bug
				bug = true;
				break;
			}
		}

		// make sure an exception is thrown
		if (bug)
			throw LogicDevsException("GVT bug detected");

		// the waiting vector now gets the accumulating vector
		waiting_vector = accumulating_vector;
		// the wait vector of the current GVT control message for this kernel is set to
		// the accumulating vector
		control_msg_.set_wait_vector(accumulating_vector);

		// now, we can clean up the accumulating vector
		accumulating_vector.clear();
	}

	// check if we are finished
	if (finished) {
		// calculate the gvt base on the clock and send value
		double gvt = std::floor(std::min(m_clock, m_send));

		// check if we're not attempting to decrease the GVT
		if (gvt < gvt_)
			throw RuntimeDevsException("GVT is decreasing");

		// update our accumulator
		accumulator_ = waiting_vector;

		// release the lock
		v_lock_.unlock();

		// set the GVT
		set_GVT(gvt, false);

		return;
	} else {
		// we are not finished,
		// so make sure we wait until the previous color messages are all received
		WaitUntilOK(prevcolor);

		// we're done waiting, update the waiting vector with this kernel's V color vector
		// for the previous color
		if (colorVectors_.find(prevcolor) != colorVectors_.end()) {
			for (auto& kernel_count_pair : colorVectors_.at(prevcolor)) {
				unsigned kernel = kernel_count_pair.first;
				int count = kernel_count_pair.second;

				// the waiting vector needs a new entry
				if (waiting_vector.find(kernel) == waiting_vector.end())
					waiting_vector[kernel] = 0;

				// add the count to the entry in the waiting vector
				waiting_vector[kernel] += count;
			}
			colorVectors_.at(prevcolor).clear();
		}

		// update the accumulating vector with this kernel's V color vector
		// for the current color
		if (colorVectors_.find(color) != colorVectors_.end()) {
			for (auto& kernel_count_pair : colorVectors_.at(color)) {
				unsigned kernel = kernel_count_pair.first;
				int count = kernel_count_pair.second;

				// the accumulating vectors needs a new entry
				if (accumulating_vector.find(kernel) == accumulating_vector.end())
					accumulating_vector[kernel] = 0;

				// add the count to the entry in the accumlating vector
				accumulating_vector[kernel] += count;
			}
			colorVectors_.at(color).clear();
		}

		// gets the local time of this kernel, this is infinity if the simulation cycle is finished
		// or the previous time
		double localtime =
		        (has_prevtime_finished_) ? std::numeric_limits<double>::infinity() : prev_time_.get_x();

		// if we are the controller then our local time should always be picked
		// if not, the minimum between the clock value from the received GVT control message
		// and the local time is picked
		double ntime = id_ ? std::min(m_clock, localtime) : localtime;

		// construct a new GVT control message
		message = GvtControlMessage(ntime, std::min(m_send, tmin_), waiting_vector, accumulating_vector);

		// set Tmin (the minimum time a red message was sent) to infinity
		tmin_ = std::numeric_limits<double>::infinity();
	}

	// update our color to the next color in the cycle
	color_ = static_cast<Color>((color_ + 1) % 4);

	// release the lock
	v_lock_.unlock();

	// send the message to the next kernel
	next_LP_->ReceiveControl(message);
}

/**
 * \brief Destructor
 */
BaseSimulator::~BaseSimulator()
{
}
}
