#include "Simulator.h"
#include <iomanip>
#include <limits>

#include <cereal/archives/binary.hpp>

namespace cdevs {
/**
 *  \brief Constructor of the simulator.
 *
 *  TODO: Actually construct a RootDEVS from the given BaseDEVS.
 *
 *  @param model a valid model (created with the provided functions)
 *  @param n_kernels The amount of kernels to run the simulation on
 */
Simulator::Simulator(std::shared_ptr<BaseDevs> model, unsigned n_kernels)
{
	sim_scheduler_type_ = SchedulerType::kMlSchedulerType;

	// simulation - general
	has_location_tracer_ = false;
	setup_ = false;

	// simulation - progress
	is_in_progress_ = false;

	// simulation - time
	gvt_interval_ = 1;
	termination_time_ = std::numeric_limits<double>::infinity();

	// checkpoint
	checkpoint_interval_ = 1;
	checkpoint_name_ = "(none)";

	// model
	model_ = model;
	is_coupled_devs_ = false;
	controller_ = Controller::create(NULL, n_kernels);
	controller_->set_classic_devs(false);

	is_flattened_ = false;

	draw_model_hierarchy_ = false;

	tracers_ = std::make_shared<Tracers>();

	n_kernels_ = n_kernels;
}

/**
 * \brief Perform startup of the simulator right before simulation
 */
void Simulator::RunStartup()
{
	// First step is to perform directconnect when the model is coupled
	if (model_->isCoupled()) {
		std::shared_ptr<CoupledDevs> coupled_model = std::dynamic_pointer_cast<CoupledDevs>(model_);
		coupled_model->SetComponentSet(coupled_model->DirectConnect());
	} else {
		for (auto& port : model_->get_input_ports()) {
			port->routing_outline_.clear();
			port->routing_inline_.clear();
		}

		for (auto& port : model_->get_output_ports()) {
			port->routing_outline_.clear();
			port->routing_inline_.clear();
		}
	}

	this->setup_ = true;
}

/**
 * \brief Create a checkpoint of this object
 */
void Simulator::Checkpoint()
{
	if (checkpoint_interval_ <= 0)
		return;

	// form file name string
	std::string full_checkpoint_name = checkpoint_name_ + "_SIM.pdc";

	// create file stream
	std::ofstream dump_stream(full_checkpoint_name);

	// flatten the model for proper serialization
	if (is_flattened_)
		model_->FlattenConnections();

	{
		cereal::BinaryOutputArchive output(dump_stream);
		output(*this);
	}

	// flattened model should be unflattened
	if (is_flattened_)
		model_->UnflattenConnections();

	dump_stream.close();
}

/**
 * \ brief Alert the Simulator that it was restored from a checkpoint
 * and thus can take some shortcuts
 */
void Simulator::LoadCheckpoint()
{
	RealSimulate();
}

void Simulator::StartAllocator()
{
	throw RuntimeDevsException("Simulator::StartAllocator not implemented yet!");
}


/**
 * \brief Start simulation with the previously set options. Can be reran afterwards to continue the simulation of
 * the model (or reinit it first and restart simulation), possibly after altering some aspects of the model with the provided methods.
 */
void Simulator::Simulate()
{
	if (!this->setup_) {
		this->RunStartup();
		std::list<std::shared_ptr<AtomicDevs> > models;

		// if we have a coupled devs, get the components and push them back
		if (model_->isCoupled()) {
			std::shared_ptr<CoupledDevs> coupled_model = std::dynamic_pointer_cast<CoupledDevs>(model_);
			for (auto& component : coupled_model->GetComponentSet())
				models.push_back(std::dynamic_pointer_cast<AtomicDevs>(component));
		} else {
			// otherwise, just push the model
			models.push_back(std::dynamic_pointer_cast<AtomicDevs>(model_));
		}

		is_flattened_ = false;
		//TODO: ALLOCATOR
		controller_->BroadcastModel(model_, models, this->sim_scheduler_type_, is_flattened_);

		// Set setup flag to prevent further startup.
		this->setup_ = true;
		tracers_->StartTracers(false);
	} else {
		tracers_->StartTracers(true);
	}

	// set allocator
	controller_->SetAllocator(allocator_);

	// set globals
	controller_->set_globals(0, 0, checkpoint_name_, tracers_);

	// if we have a termination condition, then this takes priority
	if (this->termination_condition_ != 0) {
		controller_->set_termination_condition(this->termination_condition_);
	} else {
		// otherwise, fill in the termination time
		controller_->SetTerminationTime(
		        DevsTime(this->termination_time_, std::numeric_limits<double>::infinity()));
	}

	// check if we should make checkpoints
	if (checkpoint_interval_ > 0)
		Checkpoint();

	// execute the actual simulation part
	this->RealSimulate();

	// we're finished, stop the tracers
	this->controller_->StopTracers();
}

/**
 * \brief The actual simulation part, this is identical for the 'start from scratch' and 'start from checkpoint' algorithm
 * thus it was split up
 */
void Simulator::RealSimulate()
{
	// for classic devs, we have to check whether the checkpoint_interval is set
	if(this->controller_->get_classic_devs())
	{
		// if we have a checkpoint interval, start the GVT thread
		if (checkpoint_interval_ > 0)
			controller_->StartGvtThread(gvt_interval_);
	}
	else
	{
		// in a parallel setting, we always have to run the GVT thread
		controller_->StartGvtThread(gvt_interval_);
	}

	// execute the actual simulation
	this->controller_->SimulateAll();
	// check if simulation is finished
	this->controller_->WaitFinish();
	// print out results
	this->controller_->PerformActions();
	// finish all kernels
	this->controller_->FinishAll();
	// join in main thread
	this->controller_->JoinSimulations();
}

/**
 * \brief Sets the simulation to use classic devs
 */
void Simulator::set_classic_devs()
{
	controller_->set_classic_devs(true);
}

/**
 * \brief Sets the termination condition for the simulation
 *
 * @param condition  The condition that will be checked to see if the simulation should be terminated
 */
void Simulator::set_termination_condition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> condition)
{
	termination_condition_ = condition;
}

/**
 * \brief Sets the termination time of the simulation
 *
 * @param time  The timepoint up to which the simulation is allowed to run
 */
void Simulator::set_termination_time(double time)
{
	termination_time_ = time;
	controller_->SetTerminationTime(DevsTime(time, -1)); //TODO set termination time to double?
}

/**
 * \brief Sets the checkpointing name for this simulation
 *
 * @param name The new checkpointing name
 */
void Simulator::set_checkpoint_name(std::string name)
{
	checkpoint_name_ = name;
}

/**
 * \brief Sets the checkpointing interval
 *
 * @param interval The new checkpointing interval
 */
void Simulator::set_checkpoint_interval(unsigned interval)
{
	checkpoint_interval_ = interval;
}

/**
 * \brief Sets the GVT interval
 *
 * @param interval The new GVT interval
 */
void Simulator::set_gvt_interval(unsigned interval)
{
	gvt_interval_ = interval;
}

/**
 * \brief Sets the verbosity of our simulator
 *
 * @param filename The name of the file to write to - empty is stdout
 */
void Simulator::set_verbose(std::string filename)
{
	set_tracer(std::make_shared<TracerVerbose>(controller_, filename));
}

/**
<<<<<<< HEAD
 * Adds an XmlTracer
=======
 * \brief Sets the xml output of our simulator
>>>>>>> 8a22f187e66b169ad52bb7d5f96e4442107f958e
 *
 * @param filename The name of the file to write to - empty is stdout
 */
void Simulator::set_xml(std::string filename)
{
	set_tracer(std::make_shared<TracerXml>(controller_, filename));

}

/**
<<<<<<< HEAD
 * Adds a JsonTracer
=======
 * \brief Sets the JSON output of our simulator
>>>>>>> 8a22f187e66b169ad52bb7d5f96e4442107f958e
 *
 * @param filename The name of the file to write to - empty is stdout
 */
void Simulator::set_json(std::string filename)
{
	set_tracer(std::make_shared<TracerJson>(controller_, filename));
}

/**
<<<<<<< HEAD
 * Adds a Custom Tracer
 *
 * @param tracer A shared pointer to the tracer that needs to be added
=======
 * \brief Sets a custom tracer to our simulator
 *
 * @param filename The name of the file to write to - empty is stdout
>>>>>>> 8a22f187e66b169ad52bb7d5f96e4442107f958e
 */
void Simulator::set_custom_tracer(std::shared_ptr<Tracer> tracer)
{
	tracer->set_controller(controller_);
	set_tracer(tracer);
}

/**
 * \brief Destructor
 */
Simulator::~Simulator()
{
}

/**
 * \brief Adds a tracer to our simulator
 *
 * @param tracer  The tracer to add to our simulators
 */
void Simulator::set_tracer(std::shared_ptr<Tracer> tracer)
{
	tracers_->RegisterTracer(tracer);
}

/**
 * \brief Sets the simulation scheduler to list type
 */
void Simulator::set_scheduler_list()
{
	sim_scheduler_type_ = SchedulerType::kMlSchedulerType;
}

/**
 * \brief Sets the simulation scheduler to heap type
 */
void Simulator::set_scheduler_heap()
{
	sim_scheduler_type_ = SchedulerType::kAhSchedulerType;

}

/**
 * \brief Sets the simulation scheduler to sorted list type
 */
void Simulator::set_scheduler_sorted_list()
{
	sim_scheduler_type_ = SchedulerType::kSlSchedulerType;
}

/**
 * \brief Load a previously created simulation from a saved checkpoint.
 *
 * @param name The name of the model to provide some distinction between different simulation models.
 * @param gvt The gvt of the checkpoint to load for the kernels.
 * @return A smart pointer to the Simulator object after simulation or a null pointer if no recovery was possible.
 */
std::unique_ptr<Simulator> Simulator::LoadFromCheckpoint(std::string checkpoint_name, double gvt)
{
	// form file name string
	std::string full_checkpoint_name = checkpoint_name + "_SIM.pdc";

	// create input file stream
	std::ifstream read_stream(full_checkpoint_name);
	if (!read_stream.is_open())
		return std::move(std::unique_ptr<Simulator>(nullptr));

	// creates new simulator object
	std::unique_ptr<Simulator> sim(new Simulator(std::shared_ptr<BaseDevs>(nullptr)));

	{
		cereal::BinaryInputArchive input(read_stream);
		input(*sim);
	}

	std::vector<std::ifstream> kernel_streams(sim->n_kernels_);

	for (unsigned i = 0; i < sim->n_kernels_; i++) {
		// form file name string
		std::stringstream ss;
		ss << sim->checkpoint_name_ << "_";
		ss << std::fixed;
		ss << std::setprecision(2);
		ss << std::floor(gvt * 100. + 0.5) / 100. << "_";
		ss << std::to_string(i) << ".pdc";

		std::string filename = ss.str();

		kernel_streams[i].open(filename);

		if (!kernel_streams[i].is_open())
			return std::move(std::unique_ptr<Simulator>(nullptr));
	}

	for (auto& s : kernel_streams)
		s.close();



	// load in checkpoint
	sim->LoadCheckpoint();

	return std::move(sim);
}

/**
 * \brief Gets the amount of kernels
 *
 * @return  The amount of kernels
 */
unsigned Simulator::get_n_kernels() const
{
	return n_kernels_;
}

/**
 * \brief Sets the amount of kernels
 *
 * @param  The amount of kernels
 */
void Simulator::set_n_kernels(unsigned kernels)
{
	n_kernels_ = kernels;
}

} /* namespace cdevs */
