/*
 * RootDEVS.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef ROOTDEVS_H_
#define ROOTDEVS_H_

#include "BaseDEVS.h"
#include "templates/Scheduler.h"
#include "AtomicDevs.h"
#include "utility.h"
#include <cmath>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/memory.hpp>

namespace cdevs {

class RootDevs: public BaseDevs, public std::enable_shared_from_this<RootDevs>
{
public:
	RootDevs(std::list<std::shared_ptr<AtomicDevs> > components, std::list<std::shared_ptr<BaseDevs> > models,
	        SchedulerType type);
	virtual ~RootDevs();

	void UndoDirectConnect();
	std::list<std::shared_ptr<BaseDevs> > DirectConnect();

	void set_scheduler(SchedulerType scheduler_type);
	Scheduler * get_scheduler();
	void set_gvt(double gvt, bool last_state_only);

	bool Revert(DevsTime at_time);

	void AddComponent(std::shared_ptr<AtomicDevs> component);
	std::list<std::shared_ptr<AtomicDevs> > get_component_set();

	SchedulerType get_scheduler_type();

	void FlattenConnections();
	void UnflattenConnections();
	void set_location(int location, bool force);

	int Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
	        std::list<std::shared_ptr<BaseDevs>> select_hierarchy);
	void SetTimeNext();

	std::shared_ptr<BaseDevs> getPtr();

private:
	RootDevs();

	std::list<std::shared_ptr<AtomicDevs> > components_;
	std::list<std::shared_ptr<BaseDevs> > models_;

	std::shared_ptr<Scheduler> scheduler_;
	SchedulerType scheduler_type_;

	bool direct_connected_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<BaseDevs>(this), components_, models_, scheduler_, scheduler_type_,
		        direct_connected_);
	}
};

} /* namespace ns_DEVS */

#endif /* ROOTDEVS_H_ */
